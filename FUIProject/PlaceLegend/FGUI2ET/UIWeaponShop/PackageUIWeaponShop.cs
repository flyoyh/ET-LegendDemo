/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIWeaponShop = "UIWeaponShop";
		public const string UIWeaponShop_ShopPanel = "ui://UIWeaponShop/ShopPanel";
		public const string UIWeaponShop_FixPanel = "ui://UIWeaponShop/FixPanel";
		public const string UIWeaponShop_SellPanel = "ui://UIWeaponShop/SellPanel";
		public const string UIWeaponShop_IntensifyPanel = "ui://UIWeaponShop/IntensifyPanel";
		public const string UIWeaponShop_FUIWeaponShop = "ui://UIWeaponShop/FUIWeaponShop";
	}
}