/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIMain = "UIMain";
		public const string UIMain_FUIMain = "ui://UIMain/FUIMain";
		public const string UIMain_FUIMainPanel = "ui://UIMain/FUIMainPanel";
	}
}