/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace DCET.Hotfix
{
	public static partial class FUIPackage
	{
		public const string UICharacterSelect = "UICharacterSelect";
		public const string UICharacterSelect_Mask = "ui://UICharacterSelect/Mask";
		public const string UICharacterSelect_FUICharacterSelect = "ui://UICharacterSelect/FUICharacterSelect";
	}
}