using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEngine;
using libx;


namespace ET
{
    public static class MapHelper
    {
        public static async ETVoid EnterGameAsync(Scene zoneScene, string sceneName,int index)
        {
            try
            {
                G2C_EnterMap g2CEnterMap = await zoneScene.GetComponent<SessionComponent>().Session.Call(new C2G_EnterMap { Index = index}) as G2C_EnterMap;

                UnitComponent unitComponent = zoneScene.GetComponent<UnitComponent>();
                Unit unit = EntityFactory.CreateWithId<Unit>(zoneScene,IdGenerater.Instance.GenerateId());
                unitComponent.Add(unit);
                
                unitComponent.MyUnit = unit;

                //创建及初初始化角色各组件
                unit.AddComponent(EntityFactory.Create<TCharacter, NCharacterInfo>(zoneScene, g2CEnterMap.Unit.Character));
                
                unit.AddComponent(EntityFactory.Create<BagComponent,UnitInfo>(zoneScene, g2CEnterMap.Unit));

                unit.AddComponent(EntityFactory.Create<EquipComponent,List<long>,Unit>(zoneScene, g2CEnterMap.Unit.Equips,unit));
                
                unit.AddComponent<NumericWatcherComponent>();
                //初始化属性
                NumericComponent numCmp = unit.AddComponent<NumericComponent>();
                
                
                numCmp.Set(NumericType.CurrHP,numCmp.GetAsFloat(NumericType.HP));
                numCmp.Set(NumericType.CurrMP,numCmp.GetAsFloat(NumericType.MP));
                
                string scenePath = "Assets/_Scenes/HotScene/" + sceneName + ".unity";
                //加载游戏场景
                Assets.LoadSceneAsync(scenePath,false);
                
                Game.EventSystem.Publish(new EventType.EnterMapFinish() {ZoneScene = zoneScene}).Coroutine();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }	
        }
    }
}