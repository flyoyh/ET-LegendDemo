using System;
using UnityEngine.Analytics;
using XLua;

namespace ET
{
    public static class CharacterSelectHelper
    {
        public static async ETTask<bool> CharacterCreate(Scene zoneScene,int index, string characterName,int characterClass,int gender)
        {
            try
            {
                G2C_CharacterCreate g2C_CharacterCreate = await zoneScene.GetComponent<SessionComponent>().Session.Call(new C2G_CharacterCreate() {index = index, CharacterName = characterName,CharacterClass = characterClass,Gender = gender}) as G2C_CharacterCreate;

                if (g2C_CharacterCreate.Error == ErrorCode.ERR_Success)
                {
                    Game.Scene.GetComponent<CharacterComponent>().characters.Add(g2C_CharacterCreate.Characters);
                    Game.Scene.GetComponent<CharacterComponent>().index = g2C_CharacterCreate.index;
                    return true;
                }
                else
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = zoneScene, Message = g2C_CharacterCreate.Message}).Coroutine();
                    return false;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
                return false;
            }
            
        }

       
    }
}