﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：ItemFactory
     * 创建时间：2021/7/18 星期日 21:11:56
     * 功    能：
*****************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET
{
    public static class ItemFactory
    {
        /// <summary>
        /// 新建Item
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <returns></returns>
        public static Item CreateItem(int itemConfigId)
        {
            Log.Debug("创建实体");
            //创建实体
            Item item = EntityFactory.Create<Item>(Game.Scene);
            //记录配置Id
            item.ConfigId = itemConfigId;
            item.ItemId = item.Id;
            //TODO 矿石品质几率
            if (item.Config.ItemType == "Ore")
            {
                item.OreQuality = RandomHelper.RandomNumber(1, 18);
            }

            if (item.Config.Endurance != 0)
            {
                item.MaxEndurance = item.Config.Endurance;
                //TODO测试随机耐久
                item.CurrEndurance = RandomHelper.RandomNumber(1,item.Config.Endurance + 1);
            }

            //创建时间
            item.CreateTime = TimeHelper.ServerNow();
            return item;
        }

        /// <summary>
        /// 从记录信息中创建Item
        /// </summary>
        /// <param name="itemConfigId"></param>
        /// <returns></returns>
        public static Item CreateItemFromRecord(int itemConfigId)
        {
            //创建实体
            Item item = EntityFactory.Create<Item>(Game.Scene);
            //记录配置Id
            item.ConfigId = itemConfigId;

            return item;
        }
    }
}
