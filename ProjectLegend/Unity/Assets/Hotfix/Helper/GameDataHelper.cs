﻿using FairyGUI;
using UnityEngine;

namespace ET
{
    public static class GameDataHelper
    {
        public static NTexture GetFUITexture(string path)
        {
            if(path == "") return NTexture.Empty;
            return new NTexture(AssetsHelper.LoadAssets<Texture2D>(path, AssetsType.Texture));
        }

        public static int GetFixPrice(int price,Item item, FixType type,bool add = true)
        {
            /*if (item.CurrEndurance == item.Config.Endurance)
                return price;*/
            
            if(add)
                price += item.Config.Level * GameData.FixLevelAdd * (item.MaxEndurance - item.CurrEndurance) * (type == FixType.Normal ? 1 : 3);
            else
                price -= item.Config.Level * GameData.FixLevelAdd * (item.MaxEndurance - item.CurrEndurance)* (type == FixType.Normal ? 1 : 3);
            
            return price;
        }
        
        public static int GetSellPrice(int price,Item item,bool add = true)
        {
            //如果是矿石则有品质加成
            if (item.Config.ItemType == "Ore")
            {
                if(add) 
                    price += item.Config.SellPrice * item.OreQuality;
                else 
                    price -= item.Config.SellPrice * item.OreQuality;
            }
            else if (item.ItemCount > 1)
            {
                if (add) 
                    price += item.Config.SellPrice * item.ItemCount;
                else 
                    price -= item.Config.SellPrice * item.ItemCount;
            }
            else
            {
                if (add) 
                    price += item.Config.SellPrice;
                else 
                    price -= item.Config.SellPrice;
            }


            return price;
        }
    }
}