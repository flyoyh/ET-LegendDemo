﻿namespace ET
{
    public static class ItemHelper
    {
        public static async ETTask OnUseItemAsync(Scene zoneScene, int configId,int count = 1)
        {
            await ETTask.CompletedTask;
        }
        
        public static async ETTask OnLockItemAsync(this Unit unit, long itemId)
        {
            C2M_LockItemRequest msg = new C2M_LockItemRequest{ItemId = itemId};
            M2C_LockItemResponse m2cLockItem = await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_LockItemResponse;
            
            if (m2cLockItem.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(m2cLockItem.Message);
                return;
            }

            unit.GetComponent<BagComponent>().LockBagItem(itemId);
            
            Game.EventSystem.Publish(new EventType.UpdateBag(){ZoneScene = unit.DomainScene()}).Coroutine();

            await ETTask.CompletedTask;
        }
    }
}