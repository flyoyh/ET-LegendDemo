﻿
using System.Collections.Generic;
using System.Security.Policy;

namespace ET
{
    public static class ShopHelper
    {
        public static async ETTask OnBuyItem(this Unit unit, int itemId,ShopType shopType, int itemCount = 1)
        {
            C2M_BuyItemRequest msg = new C2M_BuyItemRequest() {ItemId = itemId, ItemCount = itemCount};
            M2C_BuyItemResponse response =
                await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_BuyItemResponse;

            if (response != null && response.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(response.Message);
                return;
            }

            unit.GetComponent<BagComponent>().AddItemInfo(response.Items);
            ShopConfig config = ShopConfigCategory.Instance.GetShopConfigInItemConfigId(itemId);
            unit.UpdateGold(-(config.SellPrice * itemCount));

            Game.EventSystem.Publish(new EventType.ShowMessage() {ZoneScene = unit.DomainScene(), Message = "购买成功"}).Coroutine();
            //Game.EventSystem.Publish(new EventType.UpdateShopPanel(){ZoneScene = unit.DomainScene(),ShopType = shopType}).Coroutine();

        }

        public static async ETTask OnSellItem(this Unit unit, List<long> items)
        {
            C2M_SellItemRequest msg = new C2M_SellItemRequest() {Items = items};
            M2C_SellItemResponse response =
                await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_SellItemResponse;
            
            if(response != null && response.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(response.Message);
                return;
            }
            
            
            unit.UpdateGold(unit.GetComponent<BagComponent>().RemoveItemsById(items));
            Game.EventSystem.Publish(new EventType.UpdateSellPanel() {ZoneScene = unit.DomainScene()}).Coroutine();
            Game.EventSystem.Publish(new EventType.ShowMessage() {ZoneScene = unit.DomainScene(), Message = "出售成功"}).Coroutine();
        }

        public static async ETTask OnFixItem(this Unit unit, List<long> Items, bool normalFix)
        {
            C2M_FixEquipRequest msg = new C2M_FixEquipRequest() {Equips = Items, NormalFix = normalFix};
            M2C_FixEquipResponse response = await unit.Domain.GetComponent<SessionComponent>().Session.Call(msg) as M2C_FixEquipResponse;
            
            if(response != null && response.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(response.Message);
                return;
            }

            int gold = 0;
            for (int i = 0; i < Items.Count; i++)
            {
                gold = GameDataHelper.GetFixPrice(gold, unit.GetComponent<BagComponent>().GetItem(Items[i]),
                    normalFix ? FixType.Normal : FixType.EX, false);
            }
            //扣除金币
            unit.UpdateGold(gold);
            
            //更新装备耐久
            unit.GetComponent<BagComponent>().UpdateFixEquips(response.Equips);
            
            Game.EventSystem.Publish(new EventType.UpdateFixPanel() {ZoneScene = unit.DomainScene()}).Coroutine();
            Game.EventSystem.Publish(new EventType.ShowMessage() {ZoneScene = unit.DomainScene(), Message = "修理成功"}).Coroutine();
        }

        /// <summary>
        /// 修理全身
        /// </summary>
        public static void OnFixAll(this Unit unit, bool normalFix)
        {
            List<long> fixEquips = new List<long>();
            Item[] equips = unit.GetComponent<EquipComponent>().Equips;
            int price = 0;
            for (int i = 0; i < equips.Length; i++)
            {
                if(equips[i] == null)
                    continue;;
                if (equips[i].CurrEndurance == equips[i].MaxEndurance)
                    continue;
                //跳过护身符
                if (equips[i].Config.ItemLevel == "施法材料")
                    continue;
                
                price = GameDataHelper.GetFixPrice(price,equips[i], normalFix ? FixType.Normal : FixType.EX, true);
                fixEquips.Add(equips[i].ItemId);
            }

            if (fixEquips.Count == 0)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = unit.DomainScene(), Message = "没有可修理的装备"}).Coroutine();
                return;
            }
            
            FUIMessageBox box = Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(
                $"是否花费{price}金币普通修理全身装备","提示",MessageBoxType.Two,"修理");
            box.Two_YesBtn.self.onClick.Add(() =>
            {
                if (unit.GetComponent<TCharacter>().Gold < price)
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = unit.DomainScene(), Message = "金币不足"}).Coroutine();
                    return;
                }
                
                unit.OnFixItem(fixEquips, normalFix).Coroutine();
            });
        }
    }
}