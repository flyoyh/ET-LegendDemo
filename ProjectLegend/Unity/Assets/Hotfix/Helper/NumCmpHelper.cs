﻿namespace ET
{
    public static class NumCmpHelper
    {
        public static void UpdateNumCmp(EquipConfig config,NumericComponent numCmp,bool remove = false)
        {
            foreach (NumericType numericType in System.Enum.GetValues(typeof(NumericType)))
            {
                var info = config.GetType().GetProperty(System.Enum.GetName(typeof(NumericType),numericType));
                if(info == null) continue;

                if(remove)
                    numCmp.Set(numericType, numCmp.GetAsInt(numericType) -System.Convert.ToInt32(info.GetValue(config)));
                else
                    numCmp.Set(numericType,System.Convert.ToInt32(info.GetValue(config)) + numCmp.GetAsInt(numericType));
            }
        }
    }
}