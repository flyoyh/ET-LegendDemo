using System;
using UnityEngine;


namespace ET
{
    public static class LoginHelper
    {
        public static async ETVoid Login(Scene zoneScene, string address, string account,string password)
        {
            try
            {
                Session session = Game.Scene.GetComponent<NetKcpComponent>().Create(NetworkHelper.ToIPEndPoint("127.0.0.1:10002"));

                // 创建一个ETModel层的Session
                R2C_Login r2CLogin = (R2C_Login)await session.Call(new C2R_Login() { Account = account, Password = password });
                /*using (Session session = zoneScene.GetComponent<NetKcpComponent>().Create(NetworkHelper.ToIPEndPoint(address)))
                {
                    r2CLogin 
                }*/

                if (r2CLogin.Error == ErrorCode.ERR_Success)
                {
                    // 创建一个gate Session,并且保存到SessionComponent中
                    Session gateSession = zoneScene.GetComponent<NetKcpComponent>().Create(NetworkHelper.ToIPEndPoint(r2CLogin.Address));
                    
                    zoneScene.AddComponent<SessionComponent>().Session = gateSession;

                    G2C_LoginGate g2CLoginGate = (G2C_LoginGate)await gateSession.Call(
                        new C2G_LoginGate() { Key = r2CLogin.Key, GateId = r2CLogin.GateId,UserId = r2CLogin.UserId});
                    if (g2CLoginGate != null && g2CLoginGate.Error == ErrorCode.ERR_Success)
                    {
                        Log.Info("登陆gate成功!");
                        gateSession.AddComponent<PingComponent>();
                        await AssetsHelper.LoadSceneAsync("CharacterSelect");
                        //获取角色列表
                        await GetCharacterHelper.GetCharacter(zoneScene);
                        await Game.EventSystem.Publish(new EventType.LoginFinish() {ZoneScene = zoneScene});
                    }
                    else
                    {
                        Log.Error(g2CLoginGate.Message);
                    }
                }
                else
                {
                    Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(r2CLogin.Message);
                    //Game.EventSystem.Publish(new EventType.OneMessageBox()
                        //{ZoneScene = zoneScene, Title = "提示", Message = r2CLogin.Message}).Coroutine();
                    //og.Info(r2CLogin.Message);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public static async ETTask<bool> Register(Scene zoneScene, string address, string account,string password)
        {
            // 创建一个ETModel层的Session
            R2C_Register r2CRegister;
            using (Session session = zoneScene.GetComponent<NetKcpComponent>().Create(NetworkHelper.ToIPEndPoint(address)))
            {
                r2CRegister = (R2C_Register)await session.Call(new C2R_Register() { Account = account, Password = password });
            }

            if(r2CRegister.Error != ErrorCode.ERR_Success)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(r2CRegister.Message);
                return false;
            }
            await Game.EventSystem.Publish(new EventType.ShowMessage()
                {ZoneScene = zoneScene, Message = "注册成功"});
            return true;
        }
    }
}