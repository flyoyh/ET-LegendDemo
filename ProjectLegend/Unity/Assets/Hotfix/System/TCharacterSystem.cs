﻿namespace ET
{
    public class TCharacterAwakeSystem : AwakeSystem<TCharacter,NCharacterInfo>
    {
        public override void Awake(TCharacter self, NCharacterInfo character)
        {
            self.CharacterName = character.Name;
            self.Gender = character.Gender;
            self.CharacterClass = character.CharacterClass;
            self.Level = character.Level;
            self.Gem = character.Gem;
            self.Gold = character.Gold;
        }
    }

    public static class TCharacterSystem
    {
        
    }
}