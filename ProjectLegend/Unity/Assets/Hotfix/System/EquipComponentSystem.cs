﻿using System.Collections.Generic;



namespace ET
{
    public class EquipComponentAwakeSystem : AwakeSystem<EquipComponent,List<long>,Unit>
    {
        public override void Awake(EquipComponent self,List<long> equips,Unit unit)
        {
            BagComponent bag = unit.GetComponent<BagComponent>();
            Log.Info("初始化装备信息");
            for (int i = 0; i < equips.Count; i++)
            {
                if(equips[i] == 0) 
                    self.Equips[i] = null;
                else 
                    self.Equips[i] = bag.GetItem(equips[i]);
            }
        }
    }
    
    public class EquipComponentInitAwakeSystem : AwakeSystem<EquipComponent>
    {
        public override void Awake(EquipComponent self)
        {
            //注册角色添加装备组件时初始化装备格子
            for (int i = 0; i < self.Equips.Length; i++)
            {
                self.Equips[i] = null;
            }
        }
    }
    
    
    public static class EquipComponentSystem
    {
        public static void EquipItem(this EquipComponent self,Item item,bool isLeft = true)
        {
            EquipConfig config = EquipConfigCategory.Instance.Get(item.ConfigId);
            switch (config.Slot)
            {
                case (int)EquipSolt.RingL:
                    if (isLeft)
                        self.Equips[(int)EquipSolt.RingL] = item;
                    else
                        self.Equips[(int)EquipSolt.RingR] = item;
                    break;
                case (int)EquipSolt.BraceletL:
                    if (isLeft)
                        self.Equips[(int)EquipSolt.BraceletL] = item;
                    else
                        self.Equips[(int)EquipSolt.BraceletR] = item;
                    break;
                default:
                    self.Equips[config.Slot] = item;
                    break;
            }
            
            
        }

        public static void UnEquipItem(this EquipComponent self,long itemId)
        {
            for (int i = 0; i < self.Equips.Length; i++)
            {
                if (self.Equips[i] != null && self.Equips[i].ItemId == itemId)
                    self.Equips[i] = null;
            }
        }
        
        /// <summary>
        /// 获取指定位置装备
        /// </summary>
        public static Item GetEquip(this EquipComponent self,EquipSolt slot)
        {
            return self.Equips[(int)slot];
        }

        public static string GetEquipIcon(this EquipComponent self, EquipSolt slot)
        {
            if (self.Equips[(int) slot] != null)
                return self.Equips[(int) slot].Config.ItemIcon;
            return "";
        }

        /// <summary>
        /// 检测该装备是否已穿戴
        /// </summary>
        public static bool ContainsEquip(this EquipComponent self,long itemId)
        {
            for (int i = 0; i < self.Equips.Length; i++)
            {
                //检测该装备是否穿过
                if (self.Equips[i] != null && self.Equips[i].ItemId == itemId)
                    return true;
            }

            return false;
        }

       
    }
}