﻿namespace ET
{
    public class FUIMessageBoxAwakeSystem : AwakeSystem<FUIMessageBoxComponent,string,string,bool>
    {
        public override void Awake(FUIMessageBoxComponent self,string title,string message,bool one)
        {
            self.fui = self.GetParent<FUIMessageBox>();
        }
    }
    
    public class FUIMessageBoxDestroySystem : DestroySystem<FUIMessageBoxComponent>
    {
        public override void Destroy(FUIMessageBoxComponent self)
        {
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.One_OkBtn.self.onClick.Clear();
            self.fui.Two_NoBtn.self.onClick.Clear();
            self.fui.Two_YesBtn.self.onClick.Clear();
            self.Dispose();
        }
    }

    public static class FUIMessageBoxSystem
    {
        /// <summary>
        /// 显示信息框
        /// </summary>
        /// <param name="self"></param>
        /// <param name="title">标题</param>
        /// <param name="message">内容</param>
        /// <param name="type">类型</param>
        /// <param name="OkBtnText">确认按钮文本</param>
        /// <param name="CancelBtnText">取消按钮文本</param>
        /// <returns></returns>
        public static FUIMessageBox Show(this FUIMessageBoxComponent self, string message,string title = "",MessageBoxType type = MessageBoxType.One,string OkBtnText = "",string CancelBtnText = "")
        {
            self.InitMessageBox();
            self.fui = Game.Scene.GetComponent<FUIComponent>().Get(FUIMessageBox.UIResName) as FUIMessageBox;
            if (!string.IsNullOrEmpty(title)) self.fui.title.text = title;
            self.fui.Message.text = message;
            if (!string.IsNullOrEmpty(OkBtnText)) self.fui.Two_YesBtn.title.text = OkBtnText;
            if (!string.IsNullOrEmpty(CancelBtnText)) self.fui.Two_NoBtn.title.text = CancelBtnText;

            switch (type)
            {
                case MessageBoxType.One:
                    self.fui.ButtonC.SetSelectedIndex(0);
                    self.fui.One_OkBtn.self.onClick.Add(() =>
                    {
                        Game.Scene.GetComponent<FUIComponent>().Remove(FUIMessageBox.UIResName);
                    });
                    break;
                case MessageBoxType.Two:
                    self.fui.ButtonC.SetSelectedIndex(1);
                    self.fui.Two_YesBtn.self.onClick.Add(() =>
                    {
                        Game.Scene.GetComponent<FUIComponent>().Remove(FUIMessageBox.UIResName);
                    });
                    self.fui.Two_NoBtn.self.onClick.Add(() =>
                    {
                        Game.Scene.GetComponent<FUIComponent>().Remove(FUIMessageBox.UIResName);
                    });
                    break;
            }
            
            return self.fui;
        }

        /// <summary>
        /// 初始化信息框窗体
        /// </summary>
        /// <param name="self"></param>
        private static void InitMessageBox(this FUIMessageBoxComponent self)
        {
            var fui = FUIMessageBox.CreateInstance(Game.Scene);
            
            fui.Name = FUIMessageBox.UIResName;
            fui.self.Center();

            fui.AddComponent<FUIMessageBoxComponent>();
            Game.Scene.GetComponent<FUIComponent>().Add(fui, true);
        }
    }
}