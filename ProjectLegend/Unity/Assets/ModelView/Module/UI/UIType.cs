﻿using System;
using System.Collections.Generic;

namespace ET
{
    public static class UIType
    {
	    public const string Root = "Root";
	    public const string UILoading = "UILoading";
	    public const string UILogin = "UILogin";
		public const string UICharacterSelect = "UICharacterSelect";
		public const string UICharacterSelectItem = "UICharacterSelectItem";
		public const string UILobby = "UILobby";
    }
}