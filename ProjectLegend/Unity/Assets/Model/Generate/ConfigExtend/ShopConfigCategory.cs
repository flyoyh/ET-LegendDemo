﻿using System.Collections.Generic;

namespace ET
{
    public partial class ShopConfigCategory : ProtoObject
    {

        public List<ShopConfig> GetShopItem(int shopId)
        {
            List<ShopConfig> result = new List<ShopConfig>();
            foreach (var item in this.dict)
            {
                if(item.Value.ShopId == shopId)
                    result.Add(item.Value);
            }

            return result;
        }
        
        public ShopConfig GetShopConfigInItemConfigId(int itemId)
        {
            foreach (var item in this.dict.Values)
            {
                if (item.ItemId == itemId)
                    return item;
            }

            return null;
        }
        
    }
}