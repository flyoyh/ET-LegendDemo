using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using ProtoBuf;

namespace ET
{
    [ProtoContract]
    [Config]
    public partial class ShopConfigCategory : ProtoObject
    {
        public static ShopConfigCategory Instance;
		
        [ProtoIgnore]
        [BsonIgnore]
        private Dictionary<int, ShopConfig> dict = new Dictionary<int, ShopConfig>();
		
        [BsonElement]
        [ProtoMember(1)]
        private List<ShopConfig> list = new List<ShopConfig>();
		
        public ShopConfigCategory()
        {
            Instance = this;
        }
		
		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (ShopConfig config in list)
            {
                this.dict.Add(config.Id, config);
            }
            list.Clear();
            this.EndInit();
        }
		
        public ShopConfig Get(int id)
        {
            this.dict.TryGetValue(id, out ShopConfig item);

            if (item == null)
            {
                throw new Exception($"配置找不到，配置表名: {nameof (ShopConfig)}，配置id: {id}");
            }

            return item;
        }
		
        public bool Contain(int id)
        {
            return this.dict.ContainsKey(id);
        }

        public Dictionary<int, ShopConfig> GetAll()
        {
            return this.dict;
        }

        public ShopConfig GetOne()
        {
            if (this.dict == null || this.dict.Count <= 0)
            {
                return null;
            }
            return this.dict.Values.GetEnumerator().Current;
        }
    }

    [ProtoContract]
	public partial class ShopConfig: ProtoObject, IConfig
	{
		[ProtoMember(1, IsRequired  = true)]
		public int Id { get; set; }
		[ProtoMember(2, IsRequired  = true)]
		public int ShopId { get; set; }
		[ProtoMember(3, IsRequired  = true)]
		public string ShopName { get; set; }
		[ProtoMember(4, IsRequired  = true)]
		public int ItemId { get; set; }
		[ProtoMember(5, IsRequired  = true)]
		public int SellPrice { get; set; }
		[ProtoMember(6, IsRequired  = true)]
		public string MoneyType { get; set; }


		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            this.EndInit();
        }
	}
}
