namespace ET
{
	public static partial class OuterOpcode
	{
		 public const ushort NCharacterInfo = 20001;
		 public const ushort C2R_Login = 20002;
		 public const ushort R2C_Login = 20003;
		 public const ushort C2G_LoginGate = 20004;
		 public const ushort G2C_LoginGate = 20005;
		 public const ushort C2R_Register = 20006;
		 public const ushort M2C_AgainLogin = 20007;
		 public const ushort R2C_Register = 20008;
		 public const ushort C2G_GetCharacter = 20009;
		 public const ushort G2C_GetCharacter = 20010;
		 public const ushort C2G_CharacterCreate = 20011;
		 public const ushort G2C_CharacterCreate = 20012;
		 public const ushort C2M_TestRequest = 20013;
		 public const ushort M2C_TestResponse = 20014;
		 public const ushort C2M_LockItemRequest = 20015;
		 public const ushort M2C_LockItemResponse = 20016;
		 public const ushort C2M_UseItemRequest = 20017;
		 public const ushort M2C_UseItemResponse = 20018;
		 public const ushort C2M_EquipItemRequest = 20019;
		 public const ushort M2C_EquipItemResponse = 20020;
		 public const ushort C2M_UnEquipRequest = 20021;
		 public const ushort M2C_UnEquipResponse = 20022;
		 public const ushort C2M_BuyItemRequest = 20023;
		 public const ushort M2C_BuyItemResponse = 20024;
		 public const ushort C2M_SellItemRequest = 20025;
		 public const ushort M2C_SellItemResponse = 20026;
		 public const ushort C2M_FixEquipRequest = 20027;
		 public const ushort M2C_FixEquipResponse = 20028;
		 public const ushort Actor_TransferRequest = 20029;
		 public const ushort Actor_TransferResponse = 20030;
		 public const ushort C2G_EnterMap = 20031;
		 public const ushort G2C_EnterMap = 20032;
		 public const ushort NBagInfo = 20033;
		 public const ushort NItemInfo = 20034;
		 public const ushort UnitInfo = 20035;
		 public const ushort M2C_CreateUnits = 20036;
		 public const ushort C2M_PathfindingResult = 20037;
		 public const ushort C2M_Stop = 20038;
		 public const ushort C2M_StopTest = 20039;
		 public const ushort M2C_PathfindingResult = 20040;
		 public const ushort M2C_Stop = 20041;
		 public const ushort C2G_Ping = 20042;
		 public const ushort G2C_Ping = 20043;
		 public const ushort G2C_Test = 20044;
		 public const ushort C2M_Reload = 20045;
		 public const ushort M2C_Reload = 20046;
		 public const ushort G2C_TestHotfixMessage = 20047;
		 public const ushort C2M_TestActorRequest = 20048;
		 public const ushort M2C_TestActorResponse = 20049;
		 public const ushort PlayerInfo = 20050;
		 public const ushort PlayerInfoTest = 20051;
		 public const ushort C2G_PlayerInfo = 20052;
		 public const ushort G2C_PlayerInfo = 20053;
	}
}
