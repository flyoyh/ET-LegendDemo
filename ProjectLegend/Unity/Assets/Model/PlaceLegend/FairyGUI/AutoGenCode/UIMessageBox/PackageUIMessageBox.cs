/** This is an automatically generated class by FairyGUI plugin FGUI2ET. Please do not modify it. **/

namespace ET
{
	public static partial class FUIPackage
	{
		public const string UIMessageBox = "UIMessageBox";
		public const string UIMessageBox_FUIMessageBox = "ui://UIMessageBox/FUIMessageBox";
		public const string UIMessageBox_FUIMessage = "ui://UIMessageBox/FUIMessage";	
	}
}