﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：BagComponent
     * 创建时间：2021/7/18 星期日 21:53:28
     * 功    能：
*****************************************************/
using System.Collections.Generic;

namespace ET
{
    public class BagComponent : Entity
    {
        public int CurrSize { get; set; }
        
        public int MaxSize { get; set; }
        
        public List<Item> Items;
    }
}
