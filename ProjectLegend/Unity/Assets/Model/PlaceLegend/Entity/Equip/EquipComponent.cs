﻿using System.Collections.Generic;

namespace ET
{
    public enum EquipSolt
    {
        /// <summary>
        /// 武器
        /// </summary>
        Weapon = 0,
        /// <summary>
        /// 头盔
        /// </summary>
        Helm = 1,
        /// <summary>
        /// 项链
        /// </summary>
        Necklace = 2,
        /// <summary>
        /// 勋章（饰品）
        /// </summary>
        Medal = 3,
        /// <summary>
        /// 护甲 
        /// </summary>
        Armor = 4,
        /// <summary>
        /// 手镯左
        /// </summary>
        BraceletL = 5,
        /// <summary>
        /// 手镯右
        /// </summary>
        BraceletR = 6,
        /// <summary>
        /// 戒指左
        /// </summary>
        RingL = 7,
        /// <summary>
        /// 戒指右
        /// </summary>
        RingR = 8,
        /// <summary>
        /// 腰带
        /// </summary>
        Belt = 9,
        /// <summary>
        /// 护符
        /// </summary>
        Amulet = 10,
        /// <summary>
        /// 鞋子
        /// </summary>
        Shoes = 11,
        /// <summary>
        /// 最大格子数
        /// </summary>
        SlotMax = 12
    }
    public class EquipComponent: Entity
    {
        //当前装备
        public Item[] Equips = new Item[(int)EquipSolt.SlotMax];
    }
}