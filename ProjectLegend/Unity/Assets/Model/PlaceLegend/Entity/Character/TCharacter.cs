﻿

namespace ET
{
    public class TCharacter : Entity
    {
        public long UnitId { get; set; }
        public long DBCacheId { get; set; }
        public long UserId { get; set; }
        public string CharacterName { get; set; }
        public int CharacterClass { get; set; }
        public int Gender { get; set; }
        public int Level { get; set; }
        public long Gold { get; set; }
        public int Gem { get; set; }
    }
}
