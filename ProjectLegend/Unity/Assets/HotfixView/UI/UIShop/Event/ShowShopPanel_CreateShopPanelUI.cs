﻿using ET.EventType;

namespace ET
{
    public class ShowShopPanel_CreateShopPanelUI : AEvent<EventType.ShowShopPanel>
    {
        protected async override ETTask Run(ShowShopPanel args)
        {
            if (FUIShopPanelComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(ShopPanel.UIResName);
                return;
            }
            
            var fui = ShopPanel.CreateInstance(args.ZoneScene);

            fui.Name = ShopPanel.UIResName;

            fui.AddComponent<FUIShopPanelComponent,int>((int)args.ShopType);
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
            
            await ETTask.CompletedTask;
        }
    }
}