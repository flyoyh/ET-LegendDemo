﻿using ET.EventType;

namespace ET
{
    public class UpdateSellPanel_UpdateSellPanelUI : AEvent<EventType.UpdateSellPanel>
    {
        protected override async ETTask Run(UpdateSellPanel args)
        {
            if(FUISellPanelComponent.Instance != null)
                FUISellPanelComponent.Instance.InitSellPanel();

            await ETTask.CompletedTask;
        }
    }
}