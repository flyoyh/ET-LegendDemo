﻿using ET.EventType;

namespace ET
{
    public class ShowSellPanel_CreateSellPanelUI : AEvent<EventType.ShowSellPanel>
    {
        protected async override ETTask Run(ShowSellPanel args)
        {
            if (FUISellPanelComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(SellPanel.UIResName);
                return;
            }
            
            var fui = SellPanel.CreateInstance(args.ZoneScene);

            fui.Name = SellPanel.UIResName;

            fui.AddComponent<FUISellPanelComponent>();
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
            
            await ETTask.CompletedTask;
        }
    }
}