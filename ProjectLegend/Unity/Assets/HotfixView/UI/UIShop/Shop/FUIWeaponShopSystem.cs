﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;
using UnityEngine.Analytics;

namespace ET
{
    public class FUIWeaponShopAwakeSystem : AwakeSystem<FUIWeaponShopComponent>
    {
        public override void Awake(FUIWeaponShopComponent self)
        {
            self.fui = self.GetParent<FUIWeaponShop>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;

            FUIWeaponShopComponent.Instance = self;
            
            FUIWindowHelper.SetWindowSize(self.fui.self);
            
            //购买部分
            self.fui.ToBuyBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowShopPanel()
                    {ZoneScene = self.DomainScene(), ShopType = ShopType.WeaponShop});
                
            });
            
            //出售部分
            self.fui.ToSellBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowSellPanel() {ZoneScene = self.DomainScene()});
            });
            
            //修理部分
            self.fui.ToFixBtn.self.onClick.Set(() => self.fui.ShopPanelC.SetSelectedIndex(1));
            self.fui.FixPanel.NormalFixBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowFixPanel()
                    {ZoneScene = self.DomainScene(), FixType = FixType.Normal});
            });
            
            self.fui.FixPanel.GoodFixBtn.self.onClick.Set(() =>
            {
                Game.EventSystem.Publish(new EventType.ShowFixPanel()
                    {ZoneScene = self.DomainScene(), FixType = FixType.EX});
            });
            
            self.fui.FixPanel.CloseBtn.self.onClick.Set(() => self.fui.ShopPanelC.SetSelectedIndex(0));
            
            self.fui.FixPanel.AllNormalFixBtn.self.onClick.Set(() => self.unit.OnFixAll(true));
            self.fui.FixPanel.GoodAllFixBtn.self.onClick.Set(() => self.unit.OnFixAll(false));

            //强化部分
            self.fui.ToIntensifyBtn.self.onClick.Set(() => self.fui.ShopPanelC.SetSelectedIndex(2));
            
            self.fui.CloseBtn.self.onClick.Add(() => { FUIWindowHelper.ClosePanel(FUIWeaponShop.UIResName); });
        }
    }
    
    public class FUIWeaponShopDestroySystem : DestroySystem<FUIWeaponShopComponent>
    {
        public override void Destroy(FUIWeaponShopComponent self)
        {
            FUIWeaponShopComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.ToBuyBtn.self.onClick.Clear();
            self.fui.ToFixBtn.self.onClick.Clear();
            self.fui.ToIntensifyBtn.self.onClick.Clear();
            self.fui.ToSellBtn.self.onClick.Clear();
            self.fui.FixPanel.NormalFixBtn.self.onClick.Clear();
            self.fui.FixPanel.GoodFixBtn.self.onClick.Clear();
            self.fui.FixPanel.AllNormalFixBtn.self.onClick.Clear();
            self.fui.FixPanel.GoodAllFixBtn.self.onClick.Clear();
            self.Dispose();
        }
    }

    public static class FUIWeaponShopSystem
    {
       
 
    }
}