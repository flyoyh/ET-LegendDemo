﻿using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIShopPanelAwakeSystem : AwakeSystem<FUIShopPanelComponent,int>
    {
        public override void Awake(FUIShopPanelComponent self,int ShopId)
        {
            self.fui = self.GetParent<ShopPanel>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            FUIShopPanelComponent.Instance = self;
            
            FUIWindowHelper.SetWindowSize(self.fui.self);

            switch (ShopId)
            {
                case 1:
                    self.fui.Title.text = "药店";
                    break;
                case 2:
                    self.fui.Title.text = "杂货店";
                    break;
                case 3:
                    self.fui.Title.text = "高级杂货店";
                    break;
                case 4:
                    self.fui.Title.text = "书店";
                    break;
                case 5:
                    self.fui.Title.text = "铁匠铺";
                    break;
                case 6:
                    self.fui.Title.text = "服装店";
                    break;
                case 7:
                    self.fui.Title.text = "饰品店";
                    break;
            }
            //self.InitShopWindow(ShopId);
            self.InitShop(ShopId);
            self.fui.BuyBtn.self.onClick.Set(self.OnBuyItem);
            self.fui.ToMoreBuyBtn.self.onClick.Set(() =>
            {
                self.fui.MoreBuyC.SetSelectedIndex(1);
                self.InitMoryBuyPanel();
            });
            self.fui.MoreBuyPanel.CancelBtn.self.onClick.Set(() => self.fui.MoreBuyC.SetSelectedIndex(0));
            self.fui.CloseBtn.self.onClick.Set(() => FUIWindowHelper.ClosePanel(ShopPanel.UIResName));
            
            self.fui.MoreBuyPanel.MoreBuyBtn.self.onClick.Set(() =>
            {
                self.fui.MoreBuyC.SetSelectedIndex(0);
                self.OnMoreBuyItem();
            });
        }
    }
    
    public class FUIShopPanelDestroySystem : DestroySystem<FUIShopPanelComponent>
    {
        public override void Destroy(FUIShopPanelComponent self)
        {
            FUIShopPanelComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.BuyBtn.self.onClick.Clear();
            self.fui.MoreBuyPanel.CancelBtn.self.onClick.Clear();
            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.MoreBuyPanel.MoreBuyBtn.self.onClick.Clear();
            self.fui.ToMoreBuyBtn.self.onClick.Clear();
        }
    }

    public static class FUIShopPanelComponentSystem
    {
        //初始化武器店窗口
       public static void InitShop(this FUIShopPanelComponent self,int shopId)
        {
            self.shopId = shopId;
            self.currShopItem = null;
            self.fui.ItemList.numItems = 0;
            
            self.ShopItems = ShopConfigCategory.Instance.GetShopItem(shopId);
            
            self.fui.ItemList.SetVirtual();
            self.fui.ItemList.itemRenderer = RenderListItem;
            self.fui.ItemList.onClickItem.Add(ItemClickCallback);
            self.fui.ItemList.numItems = self.ShopItems.Count;
        }
        
        private static void RenderListItem(int index, GObject item)
        {
            GButton gItem = (GButton) item;
            ItemConfig itemConfig = ItemConfigCategory.Instance.Get(FUIShopPanelComponent.Instance.ShopItems[index].ItemId);

            gItem.GetChild("ItemIcon").asLoader.texture =
                new NTexture(AssetsHelper.LoadAssets<Texture2D>(itemConfig.ItemIcon, AssetsType.Texture));
            gItem.GetChild("ItemName").text = itemConfig.Name;
            
            if (FUIShopPanelComponent.Instance.ShopItems[index].MoneyType == "Gem")
                gItem.GetChild("Price").text = FUIShopPanelComponent.Instance.ShopItems[index].SellPrice + "元宝";
            else
                gItem.GetChild("Price").text = FUIShopPanelComponent.Instance.ShopItems[index].SellPrice + "金币";
            
        }
        
        private static void ItemClickCallback(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            int childIndex = FUIShopPanelComponent.Instance.fui.ItemList.GetChildIndex(gItem);
            FUIShopPanelComponent.Instance.currShopItem = FUIShopPanelComponent.Instance.ShopItems[childIndex];
            
            //弹出Tips
            //获取当前组件位置，并处理屏幕缩放
            Vector2 screenPos = gItem.LocalToGlobal(Vector2.zero);
            screenPos /= GRoot.inst.scale;
            //加上当前组件宽高的一半
            screenPos.x += gItem.width / 2;
            screenPos.y += gItem.height / 2;
            
            Game.EventSystem.Publish(new EventType.ShowShopTips()
                {ZoneScene = Game.Scene, ShopItem = FUIShopPanelComponent.Instance.currShopItem,ItemPostion = screenPos});
            
        }

        public static void InitMoryBuyPanel(this FUIShopPanelComponent self)
        {
            ItemConfig itemConfig = ItemConfigCategory.Instance.Get(self.currShopItem.ItemId);
            self.fui.MoreBuyPanel.ItemIcon.texture = new NTexture(AssetsHelper.LoadAssets<Texture2D>(itemConfig.ItemIcon, AssetsType.Texture));
            self.fui.MoreBuyPanel.ItemName.text = itemConfig.Name;
            self.fui.MoreBuyPanel.BuyCount.text = "1";
            self.fui.MoreBuyPanel.MaxPrice.text = self.currShopItem.SellPrice.ToString();
            self.fui.MoreBuyPanel.BuyCount.onChanged.Set(() => self.fui.MoreBuyPanel.MaxPrice.text = (self.currShopItem.SellPrice * int.Parse(self.fui.MoreBuyPanel.BuyCount.text)).ToString());
        }

        public static void OnBuyItem(this FUIShopPanelComponent self)
        {
            if (!self.CheckCanBuy(self.currShopItem, 1))
                return;
            if (self.currShopItem.MoneyType == "Gem")
            {
                FUIMessageBox box = Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(
                    $"是否花费{self.currShopItem.SellPrice}元宝购买{ItemConfigCategory.Instance.Get(self.currShopItem.ItemId).Name}","提示",MessageBoxType.Two,"购买");
                box.Two_YesBtn.self.onClick.Add(() =>
                {
                    self.unit.OnBuyItem(self.currShopItem.ItemId,(ShopType)self.shopId).Coroutine();
                });
            }
            else
            {
                FUIMessageBox box = Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(
                    $"是否花费{self.currShopItem.SellPrice}金币购买{ItemConfigCategory.Instance.Get(self.currShopItem.ItemId).Name}","提示",MessageBoxType.Two,"购买");
                box.Two_YesBtn.self.onClick.Add(() =>
                {
                    self.unit.OnBuyItem(self.currShopItem.ItemId,(ShopType)self.shopId).Coroutine();
                });
            }
            
        }
        
        public static void OnMoreBuyItem(this FUIShopPanelComponent self)
        {
            if (!self.CheckCanBuy(self.currShopItem, int.Parse(self.fui.MoreBuyPanel.BuyCount.text)))
                return;
            if (self.currShopItem.MoneyType == "Gem")
            {
                FUIMessageBox box = Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(
                    $"是否花费{self.currShopItem.SellPrice * int.Parse(self.fui.MoreBuyPanel.BuyCount.text)}元宝购买{ItemConfigCategory.Instance.Get(self.currShopItem.ItemId).Name} X {self.fui.MoreBuyPanel.BuyCount.text}","提示",MessageBoxType.Two,"购买");
                box.Two_YesBtn.self.onClick.Add(() =>
                {
                    self.unit.OnBuyItem(self.currShopItem.ItemId,(ShopType)self.shopId,int.Parse(self.fui.MoreBuyPanel.BuyCount.text)).Coroutine();
                });
            }
            else
            {
                FUIMessageBox box = Game.Scene.GetComponent<FUIMessageBoxComponent>().Show(
                    $"是否花费{self.currShopItem.SellPrice * int.Parse(self.fui.MoreBuyPanel.BuyCount.text)}金币购买{ItemConfigCategory.Instance.Get(self.currShopItem.ItemId).Name} X {self.fui.MoreBuyPanel.BuyCount.text}","提示",MessageBoxType.Two,"购买");
                box.Two_YesBtn.self.onClick.Add(() =>
                {
                    self.unit.OnBuyItem(self.currShopItem.ItemId,(ShopType)self.shopId,int.Parse(self.fui.MoreBuyPanel.BuyCount.text)).Coroutine();
                });
            }
            
        }

        public static bool CheckCanBuy(this FUIShopPanelComponent self,ShopConfig shopItem, int itemCount)
        {
            if (self.currShopItem == null)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "请选择物品购买"}).Coroutine();
                return false;
            }

            if (self.unit.GetComponent<BagComponent>().Items.Count + itemCount >
                self.unit.GetComponent<BagComponent>().CurrSize)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "背包已满"}).Coroutine();
                return false;
            }

            if (self.currShopItem.MoneyType == "Gold")
            {
                if (self.unit.GetComponent<TCharacter>().Gold < self.currShopItem.SellPrice)
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = self.DomainScene(), Message = "金币不足"}).Coroutine();
                    return false;
                }
            }
            else
            {
                if (self.unit.GetComponent<TCharacter>().Gem < self.currShopItem.SellPrice)
                {
                    Game.EventSystem.Publish(new EventType.ShowMessage()
                        {ZoneScene = self.DomainScene(), Message = "元宝不足"}).Coroutine();
                    return false;
                }
            }
            

            return true;
        }
    }
}