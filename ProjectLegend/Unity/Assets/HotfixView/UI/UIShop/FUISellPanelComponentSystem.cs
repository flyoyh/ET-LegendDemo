﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUISellPanelAwakeSystem : AwakeSystem<FUISellPanelComponent>
    {
        public override void Awake(FUISellPanelComponent self)
        {
            self.fui = self.GetParent<SellPanel>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            FUISellPanelComponent.Instance = self;
            
            FUIWindowHelper.SetWindowSize(self.fui.self);
            
            self.fui.Title.text = "出售";
            
            //self.InitShopWindow();
            self.InitSellPanel();

            self.fui.CloseBtn.self.onClick.Set(() => FUIWindowHelper.ClosePanel(SellPanel.UIResName));
            self.fui.SellBtn.self.onClick.Set(self.SellItem);

        }
    }
    
    public class FUISellPanelDestroySystem : DestroySystem<FUISellPanelComponent>
    {
        public override void Destroy(FUISellPanelComponent self)
        {
            FUISellPanelComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }
            
            self.fui.SellBtn.self.onClick.Clear();
            self.fui.CloseBtn.self.onClick.Clear();
        }
    }
    
    public static class FUISellPanelComponentSystem
    {
        //初始化武器店窗口
        public static void InitSellPanel(this FUISellPanelComponent self)
        {
            self.canSellItemList = new List<Item>();
            self.sellItemList = new List<Item>();
            self.fui.ItemList.numItems = 0;
            
            self.fui.SellPrice.text = "0";
            self.canSellItemList = self.unit.GetComponent<BagComponent>().GetUnlockItems();
            
            self.fui.ItemList.SetVirtual();
            self.fui.ItemList.itemRenderer = RenderListItem;
            self.fui.ItemList.onClickItem.Add(ItemClickCallback);
            self.fui.ItemList.numItems = GameData.BagMaxSize;
            self.fui.ItemList.SelectNone();
        }
        
        private static void RenderListItem(int index, GObject item)
        {
            GButton gItem = (GButton) item;
            if (index >= FUISellPanelComponent.Instance.canSellItemList.Count)
            {
                gItem.GetChild("icon").asLoader.texture = null;
                gItem.GetChild("count").text = "";
                return;
            }
            
            Item sellItem = FUISellPanelComponent.Instance.canSellItemList[index];

            gItem.GetChild("icon").asLoader.texture =
                new NTexture(AssetsHelper.LoadAssets<Texture2D>(sellItem.Config.ItemIcon, AssetsType.Texture));
            gItem.GetChild("count").text = sellItem.ItemCount.ToString();
           
        }
        
        private static void ItemClickCallback(EventContext context)
        {
            GComponent gItem = (GComponent) context.data;
            int childIndex = FUISellPanelComponent.Instance.fui.ItemList.GetChildIndex(gItem);

            //如果点击的物品已在出售列表，则移除
            if (FUISellPanelComponent.Instance.sellItemList.Contains(FUISellPanelComponent.Instance.canSellItemList[childIndex]))
            {
                FUISellPanelComponent.Instance.sellItemList.Remove(FUISellPanelComponent.Instance.canSellItemList[childIndex]);
                
                FUISellPanelComponent.Instance.fui.SellPrice.text =
                    GameDataHelper.GetSellPrice(int.Parse(FUISellPanelComponent.Instance.fui.SellPrice.text),
                        FUISellPanelComponent.Instance.canSellItemList[childIndex],false).ToString();
            }
            else
            {
                //出售列表新增
                FUISellPanelComponent.Instance.sellItemList.Add(FUISellPanelComponent.Instance.canSellItemList[childIndex]);

                //价格变化
                FUISellPanelComponent.Instance.fui.SellPrice.text =
                    GameDataHelper.GetSellPrice(int.Parse(FUISellPanelComponent.Instance.fui.SellPrice.text),
                        FUISellPanelComponent.Instance.canSellItemList[childIndex]).ToString();
            }
            
        }

        public static void SellItem(this FUISellPanelComponent self)
        {
            if (self.sellItemList.Count == 0)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "请选择要出售的物品"}).Coroutine();
                return;
            }

            List<long> items = new List<long>();
            for (int i = 0; i < self.sellItemList.Count; i++)
            {
                items.Add(self.sellItemList[i].ItemId);   
            }

            self.unit.OnSellItem(items);
        }
    }
}