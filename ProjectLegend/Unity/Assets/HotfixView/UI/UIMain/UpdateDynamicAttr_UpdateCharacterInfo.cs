﻿using ET.EventType;

namespace ET
{
    public class UpdateDynamicAttr_UpdateCharacterInfo : AEvent<EventType.UpdateDynamicAttr>
    {
        protected override async ETTask Run(UpdateDynamicAttr args)
        {
            FUIMainPanel fui = Game.Scene.GetComponent<FUIComponent>().Get(FUIMainPanel.UIResName) as FUIMainPanel;
            fui.GetComponent<FUIMainComponent>().UpdateCurrAttr();
            await ETTask.CompletedTask;
        }
    }
}