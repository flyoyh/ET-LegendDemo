﻿using ET.EventType;

namespace ET
{
    public class EnterMapFinish_CreateMainUI : AEvent<EventType.EnterMapFinish>
    {
        protected override async ETTask Run(EnterMapFinish args)
        {
            var fui = FUIMainPanel.CreateInstance(args.ZoneScene);

            fui.Name = FUIMainPanel.UIResName;
            fui.MakeFullScreen();

            fui.AddComponent<FUIMainComponent>();
            args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);

            await ETTask.CompletedTask;
        }
    }
}
