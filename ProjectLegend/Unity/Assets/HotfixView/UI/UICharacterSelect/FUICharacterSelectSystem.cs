﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUICharacterSelectAwakeSystem : AwakeSystem<FUICharacterSelectComponent>
    {
        public override void Awake(FUICharacterSelectComponent self)
        {
            self.fui = self.GetParent<FUICharacterSelect>();
            FUICharacterSelectComponent.Instance = self;
            //创建角色界面
            self.fui.BtnClose.self.onClick.Set(() => { self.fui.CreatePanelC.selectedIndex = 0; });
            self.fui.CreateBtn.self.onClick.Set(self.OnCreateCharacter);
            //选择职业按钮
            self.fui.Button_Z.self.onClick.Set(() => { self.characterClass = 1; });
            self.fui.ButtonF.self.onClick.Set(() => { self.characterClass = 2; });
            self.fui.Button_D.self.onClick.Set(() => { self.characterClass = 3; });
            //选择性别按钮
            self.fui.BoyBtn.self.onClick.Set(() => { self.gender = 1; });
            self.fui.GrilBtn.self.onClick.Set(() => { self.gender = 2; });

            //人物选择界面
            self.fui.StartBtn.self.onClick.Set(self.OnEnterGame);
            self.fui.ToCreateBtn.self.onClick.Set(() => { self.fui.CreatePanelC.selectedIndex = 1; });
            //选择人物按钮

            self.InitCharacterItem();
        }
    }

    public class FUICharacterSelectDestroySystem : DestroySystem<FUICharacterSelectComponent>
    {
	    public override void Destroy(FUICharacterSelectComponent self)
	    {
		    FUICharacterSelectComponent.Instance = null;
		    if (self.IsDisposed)
		    {
			    return;
		    }

		    self.Dispose();

		    self.fui.BtnClose.self.onClick.Clear();
		    self.fui.CreateBtn.self.onClick.Clear();

		    self.fui.Button_Z.self.onClick.Clear();
		    self.fui.ButtonF.self.onClick.Clear();
		    self.fui.Button_D.self.onClick.Clear();

		    self.fui.BoyBtn.self.onClick.Clear();
		    self.fui.GrilBtn.self.onClick.Clear();


		    self.fui.StartBtn.self.onClick.Clear();
		    self.fui.ToCreateBtn.self.onClick.Clear();
	    }
    }
    public static class FUICharacterSelectSystem
    {
        /*public static void SetCharacterItem(this FUICharacterSelectComponent self, UICharacterItem characterItem,NCharacterInfo character,int index)
        {
			characterItem.CharacterName.text = character.Name;
			characterItem.CharacterLevel.text = character.Level.ToString() ;
			switch(character.CharacterClass)
            {
				case 1:
					characterItem.CharacterClass.text = "战士";
					break;
				case 2:
					characterItem.CharacterClass.text = "法师";
					break;
				case 3:
					characterItem.CharacterClass.text = "道士";
					break;
			}

			if(Game.Scene.GetComponent<CharacterComponent>().index == index)
            {
				self.OnClickCharacterItem(index);
				self.fui.CharacterList.selectedIndex = index - 1;
            }
        }*/

		public static void OnEnterGame(this FUICharacterSelectComponent self)
		{
			if (Game.Scene.GetComponent<CharacterComponent>().characters.Count < self.index || self.index == 0)
			{
				Game.EventSystem.Publish(new EventType.ShowMessage()
					{ZoneScene = self.DomainScene(), Message = "请选择角色"}).Coroutine();
				return;
			}

			MapHelper.EnterGameAsync(self.DomainScene(), "Game",self.index).Coroutine();
		}

		public static async void OnCreateCharacter(this FUICharacterSelectComponent self)
        {
			if(self.characterClass == 0 || self.gender == 0)
            {
	            Game.EventSystem.Publish(new EventType.ShowMessage()
		            {ZoneScene = self.DomainScene(), Message = "请选择职业或性别"}).Coroutine();
				return;
            }
			if(string.IsNullOrEmpty(self.fui.CharacterName.text))
            {
	            Game.EventSystem.Publish(new EventType.ShowMessage()
		            {ZoneScene = self.DomainScene(), Message = "请输入角色名称"}).Coroutine();
				return;
            }
			self.index = Game.Scene.GetComponent<CharacterComponent>().characters.Count + 1;


			bool result = await CharacterSelectHelper.CharacterCreate(self.DomainScene(), self.index, self.fui.CharacterName.text, self.characterClass, self.gender);
			if(result)
            {
				self.fui.CreatePanelC.selectedIndex = 0;
				self.InitCharacterItem();
			}
        }

		/// <summary>
		/// 点击选择角色
		/// </summary>
		/// <param name="index"></param>
		public static void OnClickCharacterItem(this FUICharacterSelectComponent self, int index)
        {
			//为位置赋值
			self.index = index;
			
			List<NCharacterInfo> characters = Game.Scene.GetComponent<CharacterComponent>().characters;
			if (characters.Count < index) return;

			Texture2D texture = null ;
			//获得角色配置表
			UnitConfig config = UnitConfigCategory.Instance.Get(characters[index - 1].CharacterClass);
			switch(characters[index - 1].Gender)
            {
				case 1:
					texture = AssetsHelper.LoadAssets<Texture2D>(config.BoyIcon, AssetsType.Texture);
					break;
				case 2:
					texture = AssetsHelper.LoadAssets<Texture2D>(config.GrilIcon, AssetsType.Texture);
					break;
			}

			self.fui.CharacterIcon.texture = new NTexture(texture);
		}
		
		public static void RenderListItem(int index, GObject item)
        {
	        GButton gItem = (GButton) item;

            //如果栏位没有角色
            if (Game.Scene.GetComponent<CharacterComponent>().characters.Count - 1 < index)
            {
	            gItem.GetChild("CharacterName").text = "";
	            gItem.GetChild("CharacterLevel").text = "";
	            gItem.GetChild("CharacterClass").text = "";
                return;
            }

            NCharacterInfo info = Game.Scene.GetComponent<CharacterComponent>().characters[index];
            gItem.GetChild("CharacterName").text = info.Name;
            gItem.GetChild("CharacterLevel").text = info.Level.ToString();
            switch (info.CharacterClass)
            {
	            case 1:
		            gItem.GetChild("CharacterClass").text = "战士";
		            break;
	            case 2:
		            gItem.GetChild("CharacterClass").text = "法师";
		            break;
	            case 3:
		            gItem.GetChild("CharacterClass").text = "道士";
		            break;
            }
            
        }
        
        /// <summary>
        /// 点击物品回调
        /// </summary>
        public static void ItemClickCallback(EventContext context)
        {
            Unit unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            GComponent gItem = (GComponent) context.data;
            if (string.IsNullOrEmpty(gItem.GetChild("CharacterName").text))
            {
                return;
            }

            int childIndex = FUICharacterSelectComponent.Instance.fui.CharacterList.GetChildIndex(gItem);

            FUICharacterSelectComponent.Instance.OnClickCharacterItem(childIndex + 1);
        }

		public static void InitCharacterItem(this FUICharacterSelectComponent self)
        {
	        self.fui.CharacterList.SetVirtual();
	        self.fui.CharacterList.itemRenderer = RenderListItem;
	        self.fui.CharacterList.onClickItem.Add(ItemClickCallback);
	        self.fui.CharacterList.numItems = 4;
			CharacterComponent characterComponet = Game.Scene.GetComponent<CharacterComponent>();

			if (characterComponet.characters.Count > 0)
			{
				self.fui.CharacterList.selectedIndex = characterComponet.index - 1;
				self.OnClickCharacterItem(characterComponet.index);
			}
        }
    }
}