﻿namespace ET
{
    public class FUILoginComponentAwakeSystem : AwakeSystem<FUILoginComponent>
    {
        public override void Awake(FUILoginComponent self)
        {
            self.fuiLogin = self.GetParent<FUILoginPanel>();
            self.fuiLogin.LoginBtn.self.onClick.Set(self.OnClickLogin);
            self.fuiLogin.ToRegisterBtn.self.onClick.Set(self.OnClcikToRegister);

            self.fuiLogin.RegisterBtn.self.onClick.Set(self.OnClickRegister);
            self.fuiLogin.ReturnBtn.self.onClick.Set(self.OnClickReturn);
        }
    }

    public class FUILoginComponentDestroySystem : DestroySystem<FUILoginComponent>
    {
        public override void Destroy(FUILoginComponent self)
        {
            if (self.IsDisposed)
            {
                return;
            }

            self.Dispose();

            self.fuiLogin.LoginBtn.self.onClick.Clear();
        }
    }
    
    public static class FUILoginComponentSystem
    {
        public static void OnClickLogin(this FUILoginComponent self)
        {
            if(string.IsNullOrEmpty(self.fuiLogin.LoginAccount.text) || string.IsNullOrEmpty(self.fuiLogin.LoginPassword.text))
            {
                Game.EventSystem.Publish(new EventType.ShowMessage(){ZoneScene = self.DomainScene(), Message = "请输入用户名或密码"}).Coroutine();
                return;
            }
            
            LoginHelper.Login(self.DomainScene(), "127.0.0.1:10002",self.fuiLogin.LoginAccount.text,self.fuiLogin.LoginPassword.text).Coroutine() ; 
        }

        public static async void OnClickRegister(this FUILoginComponent self)
        {
            if (string.IsNullOrEmpty(self.fuiLogin.RegisterAccount.text) || string.IsNullOrEmpty(self.fuiLogin.RegisterPassword.text) || string.IsNullOrEmpty(self.fuiLogin.RegisterPassword_1.text))
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "请输入用户名或密码"}).Coroutine();
                return;
            }
            if(self.fuiLogin.RegisterPassword.text != self.fuiLogin.RegisterPassword_1.text)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "两次密码不一致"}).Coroutine();
                return;
            }

            bool result = await LoginHelper.Register(self.DomainScene(), "127.0.0.1:10002", self.fuiLogin.RegisterAccount.text, self.fuiLogin.RegisterPassword.text);
            if(result)
            {
                self.fuiLogin.c1.selectedIndex = 0;
            }
        }

        public static void OnClcikToRegister(this FUILoginComponent self)
        {
            self.fuiLogin.c1.selectedIndex = 1;
        }

        public static void OnClickReturn(this FUILoginComponent self)
        {
            self.fuiLogin.c1.selectedIndex = 0;
        }
    }
}