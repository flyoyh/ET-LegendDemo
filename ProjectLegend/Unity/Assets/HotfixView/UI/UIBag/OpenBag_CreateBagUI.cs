﻿using ET.EventType;
using FairyGUI;

namespace ET
{
    public class OpenBag_CreateBagUI : AEvent<EventType.OpenBag>
    {
        protected async override ETTask Run(OpenBag args)
        {
            if (FUIBagComponent.Instance != null)
            {
                FUIWindowHelper.ClosePanel(FUIBagPanel.UIResName);
                FUIMainComponent.Instance.OnCloseUI();
            }
            else
            {
                var fui = FUIBagPanel.CreateInstance(args.ZoneScene);

                fui.Name = FUIBagPanel.UIResName;
 
                fui.AddComponent<FUIBagComponent>();
                args.ZoneScene.GetComponent<FUIComponent>().Add(fui, true);
            }
           

            await ETTask.CompletedTask;
        }
    }
}