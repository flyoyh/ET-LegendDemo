﻿using ET.EventType;

namespace ET
{
    public class UpdateBag_UpdateBagUI : AEvent<EventType.UpdateBag>
    {
        protected async override ETTask Run(UpdateBag args)
        {
            if (FUIBagComponent.Instance == null) return;
            
            FUIBagComponent.Instance.InitBag();
            
            FUIMainComponent.Instance.OnOpenUI(MainUIType.Bag);
            await ETTask.CompletedTask;
        }
    }
}