﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIBagComponentAwakeSystem : AwakeSystem<FUIBagComponent>
    {
        public override void Awake(FUIBagComponent self)
        {
            self.fui = self.GetParent<FUIBagPanel>();
            self.showItems = new List<Item>();
            
            FUIWindowHelper.SetWindowSize(self.fui.self);
            
            self.fui.CloseBtn.self.onClick.Set(() =>
            {
                FUIWindowHelper.ClosePanel(FUIBagPanel.UIResName);
                FUIMainComponent.Instance.OnCloseUI();
            });

            FUIBagComponent.Instance = self;

            self.InitBag();
            
            //self.InitWindow();
        }
    }

    public class FUIBagComponentDestroySystem : DestroySystem<FUIBagComponent>
    {
        public override void Destroy(FUIBagComponent self)
        {
            FUIBagComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }

            self.fui.CloseBtn.self.onClick.Clear();
            self.Dispose();
            
        }
    }
    
    public static class FUIBagComponentSystem
    {
        public static void InitBag(this FUIBagComponent self)
        {
            if (self == null)
                return;
            self.fui.ItemList.numItems = 0;
            self.showItems.Clear();

            Unit unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            
            //排除已装备物品
            for (int i = 0; i < unit.GetComponent<BagComponent>().Items.Count; i++)
            {
                //如果该装备已穿上
                if (unit.GetComponent<EquipComponent>()
                    .ContainsEquip(unit.GetComponent<BagComponent>().Items[i].ItemId))
                    continue;
                
                self.showItems.Add(unit.GetComponent<BagComponent>().Items[i]);
            }
            
            Log.Info("初始化背包");
            self.fui.ItemList.SetVirtual();
            self.fui.ItemList.itemRenderer = RenderListItem;
            self.fui.ItemList.onClickItem.Add(ItemClickCallback);
            self.fui.ItemList.numItems = GameData.BagMaxSize;
        }
        
        /// <summary>
        /// 显示物品信息
        /// </summary>
        public static void RenderListItem(int index, GObject item)
        {
            Unit unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            GButton gItem = (GButton) item;
            //如果是未解锁的背包格子
            if (unit.GetComponent<BagComponent>().CurrSize < index)
            {
                NTexture itemIcon =
                    new NTexture(AssetsHelper.LoadAssets<Texture2D>("UI/Lock.png", AssetsType.Texture));
                gItem.GetChild("icon").asLoader.texture = itemIcon;
                gItem.name = "";
                gItem.GetChild("count").text = "";
                return;
            }
            
            if (FUIBagComponent.Instance.showItems.Count > index)
            {
                
                Item itemData = FUIBagComponent.Instance.showItems[index];
                
                NTexture itemIcon = new NTexture(AssetsHelper.LoadAssets<Texture2D>(itemData.Config.ItemIcon, AssetsType.Texture));
                gItem.GetChild("icon").asLoader.texture = itemIcon;
                gItem.name = "";
                gItem.GetChild("count").text = itemData.ItemCount.ToString();
            }
            else
            {
                gItem.icon = "";
                gItem.name = "";
                gItem.GetChild("count").text = "";
            }
        }
        
        /// <summary>
        /// 点击物品回调
        /// </summary>
        public static void ItemClickCallback(EventContext context)
        {
            Unit unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            GComponent gItem = (GComponent) context.data;
            if (gItem.GetChild("icon").asLoader.url == "")
            {
                return;
            }

            int childIndex = FUIBagComponent.Instance.fui.ItemList.GetChildIndex(gItem);
            
            //TODO 解锁格子逻辑
            if (unit.GetComponent<BagComponent>().CurrSize < childIndex)
            {
                Game.Scene.GetComponent<FUIMessageBoxComponent>().Show("当前格子未解锁");
                return;
            }
            
            //弹出Tips
            //获取当前组件位置，并处理屏幕缩放
            Vector2 screenPos = gItem.LocalToGlobal(Vector2.zero);
            screenPos /= GRoot.inst.scale;
            //加上当前组件宽高的一半
            screenPos.x += gItem.width / 2;
            screenPos.y += gItem.height / 2;
            
            Game.EventSystem.Publish(new EventType.ShowItemTips()
                {ZoneScene = Game.Scene, Item = FUIBagComponent.Instance.showItems[childIndex],ItemPostion = screenPos,ItemPanel = ItemInPanel.Bag});
        }
    }
}
