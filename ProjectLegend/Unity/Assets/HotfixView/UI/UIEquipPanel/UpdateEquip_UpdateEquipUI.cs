﻿using ET.EventType;

namespace ET
{
    public class UpdateEquip_UpdateEquipUI : AEvent<EventType.UpdateEquip>
    {
        protected async override ETTask Run(UpdateEquip args)
        {
            if (FUIEquipComponent.Instance == null) return;
            
            FUIEquipComponent.Instance.InitEquipPanel();
            FUIMainComponent.Instance.OnOpenUI(MainUIType.Equip);
            await ETTask.CompletedTask;
        }
    }
}