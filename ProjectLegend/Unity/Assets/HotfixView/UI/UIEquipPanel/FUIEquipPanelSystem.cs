﻿using System;
using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUIEquipComponentAwakeSystem : AwakeSystem<FUIEquipComponent>
    {
        public override void Awake(FUIEquipComponent self)
        {
            self.fui = self.GetParent<FUIEquipPanel>();
            self.fui.AttrPanel.CloseBtn.self.onClick.Set(() => {self.fui.AttrPanelC.SetSelectedIndex(0);});
            self.fui.AttrPanel.CloseBtnD.self.onClick.Set(() => {self.fui.AttrPanelC.SetSelectedIndex(0);});
            self.fui.CloseBtn.self.onClick.Set(() =>
            {
                FUIWindowHelper.ClosePanel(FUIEquipPanel.UIResName);
                FUIMainComponent.Instance.OnCloseUI();
            });
            
            FUIWindowHelper.SetWindowSize(self.fui.self);

            FUIEquipComponent.Instance = self;
            self.InitEquipPanel();
            //self.InitWindow();
        }
    }

    public class FUIEquipComponentDestroySystem : DestroySystem<FUIEquipComponent>
    {
        public override void Destroy(FUIEquipComponent self)
        {
            FUIEquipComponent.Instance = null;
            if (self.IsDisposed)
            {
                return;
            }

            self.fui.CloseBtn.self.onClick.Clear();
            self.fui.ToAttrBtn.self.onClick.Clear();
            self.Dispose();
           
        }
    }
    
    public static class FUIEquipPanelSystem
    {
        /// <summary>
        /// 初始化人物面板
        /// </summary>
        public static void InitEquipPanel(this FUIEquipComponent self)
        {
            if (self == null)
                return;
            self.fui.AttrPanelC.SetSelectedIndex(0);
            //GRoot.inst.CloseAllWindows();
            Log.Info("初始化人物面板");
            
            //加载角色图片
            NTexture texture = null;
            if (Game.Scene.GetComponent<UnitComponent>().MyUnit.GetComponent<TCharacter>().Gender == 1)
            {
                texture = new NTexture(
                    AssetsHelper.LoadAssets<Texture2D>(Game.Scene.GetComponent<UnitComponent>().MyUnit.Config.BoyIcon,
                        AssetsType.Texture));
            }
            else
            {
                texture = new NTexture(
                    AssetsHelper.LoadAssets<Texture2D>(Game.Scene.GetComponent<UnitComponent>().MyUnit.Config.GrilIcon,
                        AssetsType.Texture));
            }

            self.fui.CharacterIcon.texture = texture;
            
            //初始化基础属性
            Unit unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            NumericComponent numCpt = unit.GetComponent<NumericComponent>();

            //初始化面板显示
            self.fui.ADAttack.text = numCpt.GetAsFloat(NumericType.MinAD) + "-" + numCpt.GetAsFloat(NumericType.MaxAD);
            self.fui.APAttack.text = numCpt.GetAsFloat(NumericType.MinAP) + "-" + numCpt.GetAsFloat(NumericType.MaxAP);
            self.fui.DSAttack.text = numCpt.GetAsFloat(NumericType.MinDS) + "-" + numCpt.GetAsFloat(NumericType.MaxDS);
            
            
            self.fui.AttrPanel.ADAttack.text = numCpt.GetAsFloat(NumericType.MinAD) + "-" + numCpt.GetAsFloat(NumericType.MaxAD);
            self.fui.AttrPanel.APAttack.text = numCpt.GetAsFloat(NumericType.MinAP) + "-" + numCpt.GetAsFloat(NumericType.MaxAP);
            self.fui.AttrPanel.DSAttack.text = numCpt.GetAsFloat(NumericType.MinDS) + "-" + numCpt.GetAsFloat(NumericType.MaxDS);
            self.fui.AttrPanel.ADDef.text = numCpt.GetAsFloat(NumericType.MinDef) + "-" + numCpt.GetAsFloat(NumericType.MaxDef);
            self.fui.AttrPanel.APDef.text = numCpt.GetAsFloat(NumericType.MinMdef) + "-" + numCpt.GetAsFloat(NumericType.MaxMdef);
            self.fui.AttrPanel.HP.text = numCpt.GetAsFloat(NumericType.HP).ToString();
            self.fui.AttrPanel.MP.text = numCpt.GetAsFloat(NumericType.MP).ToString();
            self.fui.AttrPanel.HPRec.text = numCpt.GetAsFloat(NumericType.HPRec).ToString() + "%";
            self.fui.AttrPanel.MPRec.text = numCpt.GetAsFloat(NumericType.MPRec).ToString() + "%";
            self.fui.AttrPanel.Hit.text = numCpt.GetAsFloat(NumericType.Hit).ToString();
            self.fui.AttrPanel.Miss.text = numCpt.GetAsFloat(NumericType.Miss).ToString();
            self.fui.AttrPanel.MagicMiss.text = numCpt.GetAsFloat(NumericType.APMiss).ToString();
            self.fui.AttrPanel.Luckey.text = numCpt.GetAsFloat(NumericType.Luckey).ToString();
            self.fui.AttrPanel.Cri.text = "0";
            self.fui.AttrPanel.AtkSpeed.text = "0";
            self.fui.AttrPanel.SuckHP.text = numCpt.GetAsFloat(NumericType.SuckHP).ToString();

            //初始化装备图标
            EquipComponent equip = unit.GetComponent<EquipComponent>();

            /*for (int i = 0; i < equip.Equips.Length; i++)
            {
                if (equip.Equips[i] == null) continue;
                var slotField = self.fui.GetType().GetField(Enum.GetName(typeof(EquipSolt), i)).FieldType;
                
                foreach (var field in slotField.GetFields())
                {
                    if (field.Name == "title")
                    {
                        foreach (var title in field.FieldType.GetProperties())
                        {
                            if(title.Name == "text")
                                field.SetValue(title, "");
                        }
                    }

                    else if (field.Name == "icon")
                    {
                        foreach (var icon in field.FieldType.GetProperties())
                        {
                            if(icon.Name == "texture")
                                icon.SetValue(slotField,GameDataHelper.GetFUITexture(equip.GetEquipIcon((EquipSolt)i)));
                        }
                    }
                        
                }

            }*/
            
           
            
            if (equip.Equips[(int) EquipSolt.Helm] != null)
            {
                self.SetEquipIcon(self.fui.Helm,EquipSolt.Helm);
            }
            else
            {
                self.fui.Helm.icon.texture = null;
                self.fui.Helm.title.text = "头盔";
            }

            if (equip.Equips[(int) EquipSolt.Weapon] != null)
            {
                self.SetEquipIcon(self.fui.Weapon,EquipSolt.Weapon);
            }
            else
            {
                self.fui.Weapon.icon.texture = null;
                self.fui.Weapon.title.text = "武器";
            }

            if (equip.Equips[(int) EquipSolt.Necklace] != null)
            {
                self.SetEquipIcon(self.fui.Necklace,EquipSolt.Necklace);
            }
            else
            {
                self.fui.Necklace.icon.texture = null;
                self.fui.Necklace.title.text = "项链";
            }
            
            
            if (equip.Equips[(int) EquipSolt.Medal] != null)
            {
                self.SetEquipIcon(self.fui.Medal,EquipSolt.Medal);
            }
            else
            {
                self.fui.Medal.icon.texture = null;
                self.fui.Medal.title.text = "饰品";
            }
            
            
            if (equip.Equips[(int) EquipSolt.Armor] != null)
            {
                self.SetEquipIcon(self.fui.Armor,EquipSolt.Armor);
            }
            else
            {
                self.fui.Armor.icon.texture = null;
                self.fui.Armor.title.text = "护甲";
            }
            
            
            if (equip.Equips[(int) EquipSolt.BraceletL] != null)
            {
                self.SetEquipIcon(self.fui.BraceletL,EquipSolt.BraceletL);
            }
            else
            {
                self.fui.BraceletL.icon.texture = null;
                self.fui.BraceletL.title.text = "手镯";
            }
            
            if (equip.Equips[(int) EquipSolt.BraceletR] != null)
            {
                self.SetEquipIcon(self.fui.BraceletR,EquipSolt.BraceletR);
            }
            else
            {
                self.fui.BraceletR.icon.texture = null;
                self.fui.BraceletR.title.text = "手镯";
            }
            
            if (equip.Equips[(int) EquipSolt.RingL] != null)
            {
                self.SetEquipIcon(self.fui.RingL,EquipSolt.RingL);
            }
            else
            {
                self.fui.RingL.icon.texture = null;
                self.fui.RingL.title.text = "戒指";
            }
            
            
            if (equip.Equips[(int) EquipSolt.RingR] != null)
            {
                self.SetEquipIcon(self.fui.RingR,EquipSolt.RingR);
            }
            else
            {
                self.fui.RingR.icon.texture = null;
                self.fui.RingR.title.text = "戒指";
            }
            
            
            if (equip.Equips[(int) EquipSolt.Belt] != null)
            {
                self.SetEquipIcon(self.fui.Belt,EquipSolt.Belt);
            }
            else
            {
                self.fui.Belt.icon.texture = null;
                self.fui.Belt.title.text = "腰带";
            }
            
            
            if (equip.Equips[(int) EquipSolt.Amulet] != null)
            {
                self.SetEquipIcon(self.fui.Amulet,EquipSolt.Amulet);
            }
            else
            {
                self.fui.Amulet.icon.texture = null;
                self.fui.Amulet.title.text = "护符";
            }
            
            
            if (equip.Equips[(int) EquipSolt.Shoes] != null)
            {
                self.SetEquipIcon(self.fui.Shoes,EquipSolt.Shoes);
            }
            else
            {
                self.fui.Shoes.icon.texture = null;
                self.fui.Shoes.title.text = "鞋子";
            }
        }

        public static void ShowEquipInfo(this FUIEquipComponent self,Item item,GButton Button)
        {
            Vector2 screenPos = Button.LocalToGlobal(Vector2.zero);
            screenPos /= GRoot.inst.scale;
            //加上当前组件宽高的一半
            screenPos.x += Button.width / 2;
            screenPos.y += Button.height / 2;
            Game.EventSystem.Publish(new EventType.ShowItemTips(){ZoneScene = self.DomainScene(),Item = item,ItemPanel = ItemInPanel.Equip,ItemPostion = screenPos}).Coroutine();
        }

        /// <summary>
        /// 设置装备图标
        /// </summary>
        public static void SetEquipIcon(this FUIEquipComponent self, FUIEquipSlot slot,EquipSolt slotType)
        {
            slot.icon.texture =
                GameDataHelper.GetFUITexture(Game.Scene.GetComponent<UnitComponent>().MyUnit.GetComponent<EquipComponent>().GetEquipIcon(slotType));
            slot.title.text = "";
            slot.self.onClick.Set(() =>
            {
                self.ShowEquipInfo(Game.Scene.GetComponent<UnitComponent>().MyUnit.GetComponent<EquipComponent>().GetEquip(slotType),slot.self);
            });
            
            
        }
        
        
    }
}