﻿using FairyGUI;

namespace ET
{
    public class FUIMessageComponentAwakeSystem : AwakeSystem<FUIMessageComponent,string>
    {
        public override void Awake(FUIMessageComponent self,string message)
        {
            
            self.fui = self.GetParent<FUIMessage>();
            //self.fui.self.Center();
            self.InitWindow(message);
        }
    }
    
    public class FUIMessageComponentDestroySystem : DestroySystem<FUIMessageComponent>
    {
        public override void Destroy(FUIMessageComponent self)
        {
            if (self.IsDisposed)
            {
                return;
            }
            
            self.Dispose();
        }
    }

    public static class FUIMessageComponentSystem
    {
        public static void InitWindow(this FUIMessageComponent self,string message)
        {
            if (FUIMessageComponent.Instance != null)
            {
                GRoot.inst.ShowPopup(self.win);
            }
            else
            {
                //初始化窗口
                self.SetWindow();
                
            }
            
            self.ShowMessage(message);
            FUIMessageComponent.Instance = self;

        }
        
        public static void SetWindow(this FUIMessageComponent self)
        {
            Window win = new Window();
            win.contentPane = self.fui.self;
            win.Center();
            self.win = win;
            
            GRoot.inst.ShowPopup(win);
        }

        public async static void ShowMessage(this FUIMessageComponent self, string message)
        {
            self.fui.Message.text = message;
            await Game.Scene.GetComponent<TimerComponent>().WaitAsync(1000);
            GRoot.inst.HidePopup(self.win);
        }
    }
}