﻿using ET.EventType;
using FairyGUI;

namespace ET
{
    public class ShowMessage_CreateMessageUI : AEvent<EventType.ShowMessage>
    {
        protected async override ETTask Run(ShowMessage args)
        {
            if (FUIMessageComponent.Instance != null)
            {
                GRoot.inst.ShowPopup(FUIMessageComponent.Instance.win);
                FUIMessageComponent.Instance.ShowMessage(args.Message);
            }
            else
            {
                var fui = FUIMessage.CreateInstance(Game.Scene);

                //默认将会以Id为Name，也可以自定义Name，方便查询和管理
                fui.Name = FUIMessage.UIResName;
            
                fui.AddComponent<FUIMessageComponent,string>(args.Message);

                //Game.Scene.GetComponent<FUIComponent>().Add(fui, true);
            }
            
            await ETTask.CompletedTask;
        }
    }
}