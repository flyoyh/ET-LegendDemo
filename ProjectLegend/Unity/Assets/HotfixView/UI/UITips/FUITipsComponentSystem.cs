﻿using FairyGUI;
using UnityEngine;

namespace ET
{
    public class FUITipsComponentAwakeSystem :  AwakeSystem<FUITipsComponent, Item,Vector2,ItemInPanel>
    {
        public override void Awake(FUITipsComponent self, Item item,Vector2 itemPosition,ItemInPanel itemPanel)
        {
            self.fui = self.GetParent<FUITips>();
            self.unit = Game.Scene.GetComponent<UnitComponent>().MyUnit;
            self.fui.EquipBtn.self.onClick.Set(self.OnEquip);
            self.fui.GetBtn.self.onClick.Set(self.OnGetItem);
            self.fui.LeftBtn.self.onClick.Set(self.OnEquipLeft);
            self.fui.RightBtn.self.onClick.Set(self.OnEquipRight);
            self.fui.LockBtn.self.onClick.Set(self.OnLock);
            self.fui.UseBtn.self.onClick.Set(self.OnUseItem);
            self.fui.UnEquipBtn.self.onClick.Set(self.OnUnEquip);
            self.InitItemTips(item,itemPosition,itemPanel);
        }
    }
    
    public class FUITipsShopAwakeSystem : AwakeSystem<FUITipsComponent, ShopConfig, Vector2>
    {
        public override void Awake(FUITipsComponent self, ShopConfig shopItem, Vector2 position)
        {
            self.fui = self.GetParent<FUITips>();
            self.InitItemTips(shopItem, position,ItemInPanel.Shop);
        }
    }
    

    public class FUITipsComponentDestroySystem : DestroySystem<FUITipsComponent>
    {
        public override void Destroy(FUITipsComponent self)
        {
            if (self.IsDisposed)
            {
                FUITipsComponent.Instance = null;
                return;
            }
            
            self.fui.EquipBtn.self.onClick.Clear();
            self.fui.GetBtn.self.onClick.Clear();
            self.fui.LeftBtn.self.onClick.Clear();
            self.fui.LockBtn.self.onClick.Clear();
            self.fui.RightBtn.self.onClick.Clear();
            self.fui.UseBtn.self.onClick.Clear();
            self.fui.UnEquipBtn.self.onClick.Clear();
            self.Dispose();
        }
    }
    
    public static class FUITipsComponentSystem
    {
        /// <summary>
        /// 初始化物品信息显示
        /// </summary>
        public static void InitItemTips(this FUITipsComponent self,Item item,Vector2 itemPosition,ItemInPanel itemInPanel)
        {
            
            if (FUITipsComponent.Instance != null)
            {
                FUITipsComponent.Instance.currItem = item;
                FUITipsComponent.Instance.itemConfig = item.Config;
                self.SetPosition(itemPosition);
                GRoot.inst.ShowPopup(self.win);
                self.ShowItemInfo(itemInPanel);
                return;
            }
            
            FUITipsComponent.Instance = self;
            FUITipsComponent.Instance.currItem = item;
            FUITipsComponent.Instance.itemConfig = item.Config;
            
            //初始化窗口
            self.SetWindow(itemPosition);
            
            //显示物品信息
            self.ShowItemInfo(itemInPanel);
           
        }
        
        public static void InitItemTips(this FUITipsComponent self,ShopConfig shopItem,Vector2 itemPosition,ItemInPanel itemInPanel)
        {
            
            if (FUITipsComponent.Instance != null)
            {
                FUITipsComponent.Instance.itemConfig = ItemConfigCategory.Instance.Get(shopItem.ItemId);
                self.SetPosition(itemPosition);
                GRoot.inst.ShowPopup(self.win);
                self.ShowItemInfo(itemInPanel);
                return;
            }
            FUITipsComponent.Instance = self;
            FUITipsComponent.Instance.itemConfig = ItemConfigCategory.Instance.Get(shopItem.ItemId);
            
            //初始化窗口
            self.SetWindow(itemPosition);
            
            //显示物品信息
            self.ShowItemInfo(itemInPanel);
        }
        
        /// <summary>
        /// 初始化窗口大小及位置
        /// </summary>
        /// <param name="itemPosition"></param>
        public static void SetWindow(this FUITipsComponent self,Vector2 itemPosition)
        {
            Window win = new Window();
            win.contentPane = self.fui.self;
            
            //设置窗口宽度 高度
            FUIWindowHelper.SetWindowSize(win);
            
            self.win = win;
            
            self.SetPosition(itemPosition);
            
            GRoot.inst.ShowPopup(self.win);
            
        }
        
        /// <summary>
        /// 设置窗口位置
        /// </summary>
        /// <param name="itemPosition"></param>
        public static void SetPosition(this FUITipsComponent self,Vector2 itemPosition)
        {
            self.win.SetScale(GRoot.inst.scaleX,GRoot.inst.scaleY);
            
            //Tips超出屏幕处理
            if (itemPosition.x + self.win.width * GRoot.inst.scaleX > GRoot.inst.width)
            {
                itemPosition.x -= self.win.width * GRoot.inst.scaleX;
            }
            
            if (itemPosition.y + self.win.height * GRoot.inst.scaleY > GRoot.inst.height)
            {
                itemPosition.y -= self.win.height * GRoot.inst.scaleY;
            }
            
            //设置最终位置
            self.win.SetPosition(itemPosition.x,itemPosition.y,1f);
        }

        /// <summary>
        /// 显示物品信息
        /// </summary>
        /// <param name="itemPanel"></param>
        public static void ShowItemInfo(this FUITipsComponent self,ItemInPanel itemPanel)
        {
            self.fui.AttrList.numItems = 0;
            if(itemPanel == ItemInPanel.Shop)
                self.fui.ShowTypeC.SetSelectedIndex(6);
            //显示按钮
            else if((FUITipsComponent.Instance.itemConfig.ItemLevel == "戒指" || FUITipsComponent.Instance.itemConfig.ItemLevel == "手镯") && itemPanel == ItemInPanel.Bag)
                self.fui.ShowTypeC.SetSelectedIndex(4);
            
            else if(itemPanel == ItemInPanel.Bag && FUITipsComponent.Instance.itemConfig.CanUse)
                self.fui.ShowTypeC.SetSelectedIndex(1);
            
            else if(FUITipsComponent.Instance.itemConfig.ItemType == "Equip" && itemPanel == ItemInPanel.Bag)
                self.fui.ShowTypeC.SetSelectedIndex(2);
            
            else if(itemPanel == ItemInPanel.Box)
                self.fui.ShowTypeC.SetSelectedIndex(5);
            
            else if(itemPanel == ItemInPanel.Equip)
                self.fui.ShowTypeC.SetSelectedIndex(3);
            else
                self.fui.ShowTypeC.SetSelectedIndex(0);
            
            
            //显示基本信息
            self.fui.ItemName.text = FUITipsComponent.Instance.itemConfig.Name;
            self.fui.ItemType.text = FUITipsComponent.Instance.itemConfig.ItemLevel;
            
            self.fui.ItemIcon.texture = new NTexture(AssetsHelper.LoadAssets<Texture2D>(FUITipsComponent.Instance.itemConfig.ItemIcon, AssetsType.Texture));
            if(FUITipsComponent.Instance.itemConfig.ItemType == "Ore" && itemPanel != ItemInPanel.Shop)
                self.fui.SellPrice.text = "回收价格:" + FUITipsComponent.Instance.itemConfig.SellPrice * FUITipsComponent.Instance.currItem.OreQuality;
            else
                self.fui.SellPrice.text = "回收价格:" + FUITipsComponent.Instance.itemConfig.SellPrice;

            if (itemPanel == ItemInPanel.Shop)
                self.fui.SellPrice.text = "";
            
            //商店界面显示配置表耐久
            if (itemPanel == ItemInPanel.Shop)
                self.fui.Endurance.text = "耐久:" + FUITipsComponent.Instance.itemConfig.Endurance + "/" +
                                          FUITipsComponent.Instance.itemConfig.Endurance;
            
            //如果不是商店界面 并且耐久不为0，显示自身耐久
            if(FUITipsComponent.Instance.itemConfig.Endurance != 0 && itemPanel != ItemInPanel.Shop)
                self.fui.Endurance.text = "耐久:" + FUITipsComponent.Instance.currItem.CurrEndurance + "/" + FUITipsComponent.Instance.currItem.MaxEndurance;

            
            
            self.fui.ItemLevel.text = "需要等级:" + FUITipsComponent.Instance.itemConfig.Level;
            
            switch (FUITipsComponent.Instance.itemConfig.AstrictClass)
            {
                case 0:
                    self.fui.ItemClass.text = "";
                    break;
                case 1:
                    self.fui.ItemClass.text = "需要职业:战士";
                    break;
                case 2:
                    self.fui.ItemClass.text = "需要职业:法师";
                    break;
                case 3:
                    self.fui.ItemClass.text = "需要职业:道士";
                    break;
            }

            if (FUITipsComponent.Instance.currItem != null)
            {
                if (FUITipsComponent.Instance.currItem.IsLock)
                    self.fui.LockBtn.title.text = "解锁";
                else
                    self.fui.LockBtn.title.text = "锁定";
            }

            //显示普通物品信息
            if (FUITipsComponent.Instance.itemConfig.ItemType == "Item")
            {
                self.fui.AttrList.itemRenderer = ItemRenderer;
                self.fui.AttrList.numItems = 1;
                self.fui.AttrList.ResizeToFit();
            }
            
            //显示矿石信息
            if (FUITipsComponent.Instance.itemConfig.ItemType == "Ore" && itemPanel != ItemInPanel.Shop)
            {
                GComponent oreItem = UIPackage.CreateObject(TipsCmp.UIPackageName, TipsCmp.UIResName).asCom;
                oreItem.GetChild("AttrText").text = FUITipsComponent.Instance.itemConfig.Descirption;
                self.fui.AttrList.AddChild(oreItem);
                
                GComponent oreItem1 = UIPackage.CreateObject(TipsCmp.UIPackageName, TipsCmp.UIResName).asCom;
                oreItem1.GetChild("AttrText").text = "品质 " + FUITipsComponent.Instance.currItem.OreQuality;
                self.fui.AttrList.AddChild(oreItem1);
                
                self.fui.AttrList.ResizeToFit();
            }
            //显示高级杂货店金矿品质
            else if (FUITipsComponent.Instance.itemConfig.ItemType == "Ore" && itemPanel == ItemInPanel.Shop)
            {
                GComponent oreItem = UIPackage.CreateObject(TipsCmp.UIPackageName, TipsCmp.UIResName).asCom;
                oreItem.GetChild("AttrText").text = FUITipsComponent.Instance.itemConfig.Descirption;
                self.fui.AttrList.AddChild(oreItem);
                
                GComponent oreItem1 = UIPackage.CreateObject(TipsCmp.UIPackageName, TipsCmp.UIResName).asCom;
                oreItem1.GetChild("AttrText").text = "品质 15";
                self.fui.AttrList.AddChild(oreItem1);
                
                self.fui.AttrList.ResizeToFit();
            }

            if (FUITipsComponent.Instance.itemConfig.ItemType == "Equip")
            {
                if (!FUITipsComponent.Instance.itemConfig.Descirption.Contains("|"))
                {
                    self.fui.AttrList.itemRenderer = ItemRenderer;
                    self.fui.AttrList.numItems = 1;
                    self.fui.AttrList.ResizeToFit();
                }
                else
                {
                    
                    string[] str = FUITipsComponent.Instance.itemConfig.Descirption.Split('|');
                    for (int i = 0; i < str.Length; i++)
                    {
                        GComponent oreItem = UIPackage.CreateObject(TipsCmp.UIPackageName, TipsCmp.UIResName).asCom;
                        oreItem.GetChild("AttrText").text = str[i];
                        self.fui.AttrList.AddChild(oreItem);
                    }
                    self.fui.AttrList.ResizeToFit();
                }
            }
        }
        
        public static void ItemRenderer(int index, GObject item)
        {
            GComponent gItem = item.asCom;
            gItem.GetChild("AttrText").text = FUITipsComponent.Instance.itemConfig.Descirption;
        }

        public static bool CheckLevel(this FUITipsComponent self)
        {
            TCharacter character = self.unit.GetComponent<TCharacter>();
            if (FUITipsComponent.Instance.itemConfig.Level > character.Level)
            {
                Game.EventSystem.Publish(new EventType.ShowMessage()
                    {ZoneScene = self.DomainScene(), Message = "等级不足"});
                return false;
            }

            return true;
        }
        
        public static async void OnEquip(this FUITipsComponent self)
        {
            //if (!CheckLevel())
                //return;

            await self.unit.OnEquipAsync(FUITipsComponent.Instance.currItem.ItemId);
            GRoot.inst.HidePopup(self.win);
            
        }

        public static async void OnEquipLeft(this FUITipsComponent self)
        {
            //if (!CheckLevel())
                //return;

            await self.unit.OnEquipAsync(FUITipsComponent.Instance.currItem.ItemId,true);
            GRoot.inst.HidePopup(self.win);
        }
        
        public static async void OnEquipRight(this FUITipsComponent self)
        {
            //if (!CheckLevel())
                //return;

            await self.unit.OnEquipAsync(FUITipsComponent.Instance.currItem.ItemId,false);
            GRoot.inst.HidePopup(self.win);
        }

        public static async void OnUnEquip(this FUITipsComponent self)
        {
            await self.unit.OnUnEquipAsync(FUITipsComponent.Instance.currItem.ItemId);
            GRoot.inst.HidePopup(self.win);
        }
        
        public static async void OnUseItem(this FUITipsComponent self)
        {
            //TODO 使用物品逻辑
            await ItemHelper.OnUseItemAsync(self.DomainScene(), FUITipsComponent.Instance.itemConfig.Id);
            GRoot.inst.HidePopup(self.win);
        }

        public static async void OnLock(this FUITipsComponent self)
        {
           await self.unit.OnLockItemAsync(FUITipsComponent.Instance.currItem.ItemId);
           GRoot.inst.HidePopup(self.win);
        }
        

        public static void OnGetItem(this FUITipsComponent self)
        {
            
        }
    }
}