syntax = "proto3";
package ET;

message NCharacterInfo // IMessage
{
	string Name = 1;
	int32 Gender = 2;
	int32 CharacterClass = 3;
	int64 Gold = 4;
	int32 Gem = 5;
	int32 Level = 6;
}

//ResponseType R2C_Login
message C2R_Login // IRequest
{
	int32 RpcId = 90;
	string	Account	 = 1;	// 帐号
	string	Password = 2;	// 密码
}

message R2C_Login // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	string Address = 1;
	int64 Key	    = 2;
	int64 GateId = 3;
}

//ResponseType G2C_LoginGate
message C2G_LoginGate // IRequest
{
	int32 RpcId = 90;
	int64 Key = 1;	// 帐号
	int64 GateId = 2;
}

message G2C_LoginGate // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	int64 UserId = 1;
}

//ResponseType R2C_Register
message C2R_Register // IRequest
{
	int32 RpcId = 90;
	string Account = 1;	// 帐号
	string Password = 2;	// 密码
}

message R2C_Register // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType G2C_GetCharacter
message C2G_GetCharacter // IRequest
{
	int32 RpcId = 90;
}

message G2C_GetCharacter // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	repeated NCharacterInfo Characters = 1;
	int32 index = 2;
}

//ResponseType G2C_CharacterCreate
message C2G_CharacterCreate // IRequest
{
	int32 RpcId = 90;
	int32 index = 1;
	string CharacterName = 2;
	int32 CharacterClass = 3;
	int32 Gender = 4;
}

message G2C_CharacterCreate // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	int32 index = 1;
	NCharacterInfo Characters = 2;
}


//ResponseType M2C_TestResponse
message C2M_TestRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	string request = 1;
}

message M2C_TestResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	string response = 1;
}

//ResponseType M2C_LockItemResponse
message C2M_LockItemRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int64 ItemId = 1;
}

message M2C_LockItemResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType M2C_UseItemResponse
message C2M_UseItemRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int32 ConfigId = 1;
}

message M2C_UseItemResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType M2C_EquipItemResponse
message C2M_EquipItemRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int64 ItemId = 1;
	bool IsLeft = 2;
}

message M2C_EquipItemResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}


//ResponseType M2C_UnEquipResponse
message C2M_UnEquipRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int64 ItemId = 1;
}

message M2C_UnEquipResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType M2C_BuyItemResponse
message C2M_BuyItemRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int32 ItemId = 1;
	int32 ItemCount = 2;
}

message M2C_BuyItemResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	repeated NItemInfo Items = 1;
}

//ResponseType M2C_SellItemResponse
message C2M_SellItemRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	repeated int64 Items = 1;
}

message M2C_SellItemResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType M2C_FixEquipResponse
message C2M_FixEquipRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	repeated int64 Equips = 1;
	bool NormalFix = 2;
}

message M2C_FixEquipResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	repeated NItemInfo Equips = 1;
}

//ResponseType Actor_TransferResponse
message Actor_TransferRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 93;
	int32 MapIndex = 1;
}

message Actor_TransferResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}

//ResponseType G2C_EnterMap
message C2G_EnterMap // IRequest
{
	int32 RpcId = 90;
	int32 Index = 1;
}

message G2C_EnterMap // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	// 自己的unit id
	int64 UnitId = 1;
	// 所有的unit
	UnitInfo Unit = 2;
}

message NBagInfo
{
	int32 CurrSize = 1;
	repeated NItemInfo Items = 2;
}

message NItemInfo
{
	int64 ItemId = 1;
	int32 ConfigId = 2;
	int32 ItemCount = 4;
	int64 CreateTime = 5;
	int32 CurrEndurance = 6;
	int32 MaxEndurance = 7;
	int32 OreQuality = 8;
	bool IsLock = 9;
}


message UnitInfo
{
	int64 UnitId  = 1;
    int32 ConfigId = 2;
	NCharacterInfo Character = 3;
	NBagInfo Bag = 4;
	repeated int64 Equips = 5;
}

message M2C_CreateUnits // IActorMessage
{
	int32 RpcId = 90;
	int64 ActorId = 93;
    repeated UnitInfo Units = 2;
}

message C2M_PathfindingResult // IActorLocationMessage
{
	int32 RpcId = 90;
	int64 ActorId = 93;
		
	float X = 1;
	float Y = 2;
	float Z = 3;
}

message C2M_Stop // IActorLocationMessage
{    
	int32 RpcId = 90;
	int64 ActorId = 93;
}

message C2M_StopTest // IActorLocationMessage
{    
	int32 RpcId = 90;
	int64 ActorId = 93;
}


message M2C_PathfindingResult // IActorMessage
{
	int64 ActorId = 93;
	
	int64 Id = 1;

	float X = 2;
	float Y = 3;
	float Z = 4;
	
	repeated float Xs = 5;
	repeated float Ys = 6;
	repeated float Zs = 7;
}

message M2C_Stop // IActorMessage
{
	int32 Error = 1;
	
	int64 Id = 2;
	float X = 3;
    float Y = 4;
    float Z = 5;
    
    float A = 6;
    float B = 7;
    float C = 8;
    float W = 9;
}

//ResponseType G2C_Ping
message C2G_Ping // IRequest
{
	int32 RpcId = 90;
}

message G2C_Ping // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	
	int64 Time = 1;
}


message G2C_Test // IMessage
{
}

//ResponseType M2C_Reload
message C2M_Reload // IRequest
{
	int32 RpcId = 90;
	string Account = 1;
	string Password = 2;
}

message M2C_Reload // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
}


message G2C_TestHotfixMessage // IMessage
{
	string Info = 1;
}

//ResponseType M2C_TestActorResponse
message C2M_TestActorRequest // IActorLocationRequest
{
	int32 RpcId = 90;
	int64 ActorId = 91;
	string Info = 1;
}

message M2C_TestActorResponse // IActorLocationResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	string Info = 1;
}

message PlayerInfo // IMessage
{
	int32 RpcId = 90;
}

message PlayerInfoTest // IMessage
{
	int32 RpcId = 90;
}

//ResponseType G2C_PlayerInfo
message C2G_PlayerInfo // IRequest
{
	int32 RpcId = 90;
}

message G2C_PlayerInfo // IResponse
{
	int32 RpcId = 90;
	int32 Error = 91;
	string Message = 92;
	PlayerInfo PlayerInfo = 1;
	repeated PlayerInfo PlayerInfos = 2;
    repeated string TestRepeatedString = 3;
    repeated int32 TestRepeatedInt32 = 4;
    repeated int64 TestRepeatedInt64 = 5;
}