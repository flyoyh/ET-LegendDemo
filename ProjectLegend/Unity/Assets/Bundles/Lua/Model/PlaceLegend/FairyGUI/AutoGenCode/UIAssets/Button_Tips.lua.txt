-- Generated by CSharp.lua Compiler
local System = System
local FairyGUI = FairyGUI
local ET
System.import(function (out)
  ET = out.ET
end)
System.namespace("ET", function (namespace)
  namespace.class("Button_TipsAwakeSystem", function (namespace)
    local Awake
    Awake = function (this, self, go)
      self:Awake(go)
    end
    return {
      base = function (out)
        return {
          out.ET.AwakeSystem_2(out.ET.Button_Tips, out.FairyGUI.GObject)
        }
      end,
      Awake = Awake,
      __metadata__ = function (out)
        return {
          class = { 0x6, out.ET.ObjectSystemAttribute() }
        }
      end
    }
  end)

  namespace.class("Button_Tips", function (namespace)
    local CreateGObject, CreateGObjectAsync, CreateInstance, CreateInstanceAsync, Create1, GetFormPool, Awake, Dispose, 
    class
    CreateGObject = function ()
      return FairyGUI.UIPackage.CreateObject("UIAssets" --[[Button_Tips.UIPackageName]], "Button_Tips" --[[Button_Tips.UIResName]])
    end
    CreateGObjectAsync = function (result)
      FairyGUI.UIPackage.CreateObjectAsync("UIAssets" --[[Button_Tips.UIPackageName]], "Button_Tips" --[[Button_Tips.UIResName]], result)
    end
    CreateInstance = function (domain)
      return ET.EntityFactory.Create1(domain, CreateGObject(), false, class, FairyGUI.GObject)
    end
    CreateInstanceAsync = function (domain)
      local tcs = System.TaskCompletionSource()

      CreateGObjectAsync(function (go)
        tcs:SetResult(ET.EntityFactory.Create1(domain, go, false, class, FairyGUI.GObject))
      end)

      return tcs:getTask()
    end
    Create1 = function (domain, go)
      return ET.EntityFactory.Create1(domain, go, false, class, FairyGUI.GObject)
    end
    -- <summary>
    -- 通过此方法获取的FUI，在Dispose时不会释放GObject，需要自行管理（一般在配合FGUI的Pool机制时使用）。
    -- </summary>
    GetFormPool = function (domain, go)
      local fui = ET.GObjectHelper.Get(go, class)

      if fui == nil then
        fui = Create1(domain, go)
      end

      fui.isFromFGUIPool = true

      return fui
    end
    Awake = function (this, go)
      if go == nil then
        return
      end

      this.GObject = go

      if System.String.IsNullOrWhiteSpace(this:getName()) then
        this:setName(this.Id:ToString())
      end

      this.self = System.cast(FairyGUI.GButton, go)

      ET.GObjectHelper.Add(this.self, this)

      local com = go:getasCom()

      if com ~= nil then
        this.button = com:GetController("button")
        this.n4 = System.cast(FairyGUI.GGraph, com:GetChild("n4"))
        this.n0 = System.cast(FairyGUI.GImage, com:GetChild("n0"))
        this.title = System.cast(FairyGUI.GTextField, com:GetChild("title"))
      end
    end
    Dispose = function (this)
      if this:getIsDisposed() then
        return
      end

      System.base(this).Dispose(this)

      ET.GObjectHelper.Remove(this.self)
      this.self = nil
      this.button = nil
      this.n4 = nil
      this.n0 = nil
      this.title = nil
    end
    class = {
      base = function (out)
        return {
          out.ET.FUI
        }
      end,
      CreateInstance = CreateInstance,
      CreateInstanceAsync = CreateInstanceAsync,
      Create1 = Create1,
      GetFormPool = GetFormPool,
      Awake = Awake,
      Dispose = Dispose
    }
    return class
  end)
end)
