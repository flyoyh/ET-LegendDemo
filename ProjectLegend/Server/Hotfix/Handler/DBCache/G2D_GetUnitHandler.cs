using System;
using System.Collections.Generic;

namespace ET
{
    public class G2D_GetUnitHandler : AMActorRpcHandler<Scene, G2D_GetUnit,D2G_GetUnit>
    {
        protected override async ETTask Run(Scene scene, G2D_GetUnit request, D2G_GetUnit response, Action reply)
        {
            try
            {
                DBCacheComponent db = scene.Domain.GetComponent<DBCacheComponent>();
                
                TCharacter character = await db.Query<TCharacter>(request.CharacterId);
                BagComponent bag = await db.Query<BagComponent>(request.CharacterId);
                EquipComponent equip = await db.Query<EquipComponent>(request.CharacterId);

                List<Entity> Components = new List<Entity>();
                Components.Add(character);
                Components.Add(bag);
                Components.Add(equip);

                response.Components = Components;
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
        }
    }
}