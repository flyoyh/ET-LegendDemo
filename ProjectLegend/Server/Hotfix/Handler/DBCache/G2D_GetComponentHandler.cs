﻿using System;

namespace ET
{
    public class G2D_GetComponentHandler : AMActorRpcHandler<Scene, G2D_GetComponent,D2G_GetComponent>
    {
        protected override async ETTask Run(Scene scene, G2D_GetComponent request, D2G_GetComponent response, Action reply)
        {
            try
            {
                DBCacheComponent db = scene.Domain.GetComponent<DBCacheComponent>();
                Entity entity = null;
                switch (request.Component)
                {
                    case "TCharacter":
                        entity = await db.Query<TCharacter>(request.CharacterId);
                        break;
                    case "BagComponent":
                        entity = await db.Query<BagComponent>(request.CharacterId);
                        break;
                    case "EquipComponent":
                        entity = await db.Query<BagComponent>(request.CharacterId);
                        break;
                }
                
                response.Component = entity;
                
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
        }
    }
}