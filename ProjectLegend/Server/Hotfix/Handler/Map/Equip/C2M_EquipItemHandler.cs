
using System;

namespace ET
{

    [ActorMessageHandler]
    public class C2M_EquipItemHandler : AMActorLocationRpcHandler<Unit, C2M_EquipItemRequest, M2C_EquipItemResponse>
    {
        protected async override ETTask Run(Unit unit, C2M_EquipItemRequest request, M2C_EquipItemResponse response,
            Action reply)
        {
            try
            {
                //获取物品
                BagComponent bag = unit.GetComponent<BagComponent>();
                Item item = bag.GetItem(request.ItemId);

                if (item == null)
                {
                    response.Error = ErrorCode.ERR_Error;
                    response.Message = "没有该装备";
                    reply();
                }

                //获取物品信息
                EquipComponent equip = unit.GetComponent<EquipComponent>();
                

                EquipConfig config = EquipConfigCategory.Instance.Get(item.ConfigId);
                NumericComponent numCmp = unit.GetComponent<NumericComponent>();

                //判断当前是否有装备
                Item currEquip = null;
                switch (config.Slot)
                {
                    case (int) EquipSolt.RingL:
                        if (request.IsLeft)
                            currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.RingL);
                        else
                            currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.RingR);
                        break;
                    case (int) EquipSolt.BraceletL:
                        if (request.IsLeft)
                            currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.BraceletL);
                        else
                            currEquip = unit.GetComponent<EquipComponent>().GetEquip(EquipSolt.BraceletR);
                        break;
                    default:
                        currEquip = unit.GetComponent<EquipComponent>().GetEquip((EquipSolt) config.Slot);
                        break;
                }

                //如果有装备则移除该装备属性
                if (currEquip != null)
                {
                    EquipConfig currEquipConfig = EquipConfigCategory.Instance.Get(currEquip.ConfigId);
                    NumCmpHelper.UpdateNumCmp(currEquipConfig, numCmp, true);
                }

                equip.EquipItem(item);

                //为角色添加装备属性
                NumCmpHelper.UpdateNumCmp(config, numCmp);
                
                reply();
                
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }

        }
    }

    [ActorMessageHandler]
    public class C2M_UnEquipItemHandler : AMActorLocationRpcHandler<Unit, C2M_UnEquipRequest, M2C_UnEquipResponse>
    {
        protected async override ETTask Run(Unit unit, C2M_UnEquipRequest request, M2C_UnEquipResponse response, Action reply)
        {
            try
            {
                //获取物品
                BagComponent bag = unit.GetComponent<BagComponent>();
                Item item = bag.GetItem(request.ItemId);

                if (item == null)
                {
                    response.Error = ErrorCode.ERR_Error;
                    response.Message = "没有该装备";
                    reply();
                }

                //获取物品信息
                EquipComponent equip = unit.GetComponent<EquipComponent>();
                EquipConfig config = EquipConfigCategory.Instance.Get(item.ConfigId);
                NumericComponent numCmp = unit.GetComponent<NumericComponent>();
                
                //移除装备，移除属性
                equip.UnEquipItem(item.Id);
                NumCmpHelper.UpdateNumCmp(config,numCmp,true);
                
                reply();
                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response, e, reply);
            }
            
        }
    }

}
