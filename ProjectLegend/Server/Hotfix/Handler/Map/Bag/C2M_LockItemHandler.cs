using System;
namespace ET
{
    
    [ActorMessageHandler]
    public class C2M_LockItemHandler : AMActorLocationRpcHandler<Unit, C2M_LockItemRequest, M2C_LockItemResponse>
    {
        protected override async ETTask Run(Unit unit, C2M_LockItemRequest message, M2C_LockItemResponse response, Action reply)
        {
            try
            {
                BagComponent bag = unit.GetComponent<BagComponent>();
                if (!bag.LockBagItem(message.ItemId))
                {
                    response.Error = ErrorCode.ERR_Error;
                    response.Message = "没有该物品";
                }
                else
                {
                    response.Error = ErrorCode.ERR_Success;
                }
            
                reply();

                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                ReplyError(response,e,reply);
            }
            
        }
    }
}