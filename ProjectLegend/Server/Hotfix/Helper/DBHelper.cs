﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：DBHelper
     * 创建时间：2021/7/18 星期日 20:27:59
     * 功    能：
*****************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET
{
    public static class DBHelper
    {
        public static async ETTask<T> GetDBItem<T>(long id) where T : Entity
        {
            DBComponent db = Game.Scene.GetComponent<DBComponent>();
            T item = await db.Query<T>(id);
            return item;
        }

        public static async ETTask UpdateCacheDB(Unit unit)
        {
            List<Entity> components = new List<Entity>();
            TCharacter character = unit.GetComponent<TCharacter>();
            components.Add(unit.GetComponent<BagComponent>());
            components.Add(unit.GetComponent<EquipComponent>());
            components.Add(character);
            
            D2M_SaveUnit response = (D2M_SaveUnit) await ActorMessageSenderComponent.Instance.Call(GetDbCacheId(unit), new M2D_SaveUnit(){CharacterId = character.Id,Components = components});
            if (response.Error != ErrorCode.ERR_Success)
            {
                Log.Error("更新缓存服Unit数据出错");
            }
            
            await ETTask.CompletedTask;
        }
        
        public static long GetDbCacheId(Unit unit)
        {
            return StartSceneConfigCategory.Instance.GetBySceneName(unit.DomainZone() ,Enum.GetName(SceneType.DBCache)).SceneId;
        }
    }
}
