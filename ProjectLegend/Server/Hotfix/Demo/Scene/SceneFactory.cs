

using System.Net;

namespace ET
{
    public static class SceneFactory
    {
        public static async ETTask<Scene> Create(Entity parent, string name, SceneType sceneType)
        {
            long id = IdGenerater.Instance.GenerateId();
            return await Create(parent, id, parent.DomainZone(), name, sceneType);
        }
        
        public static async ETTask<Scene> Create(Entity parent, long id, int zone, string name, SceneType sceneType, StartSceneConfig startSceneConfig = null)
        {
            await ETTask.CompletedTask;
            Scene scene = EntitySceneFactory.CreateScene(id, zone, sceneType, name);
            scene.Parent = parent;

            scene.AddComponent<MailBoxComponent, MailboxType>(MailboxType.UnOrderMessageDispatcher);

            switch (scene.SceneType)
            {
                case SceneType.Realm:
                    scene.AddComponent<NetKcpComponent, IPEndPoint>(startSceneConfig.OuterIPPort);
                    scene.AddComponent<DBComponent,StartZoneConfig>(startSceneConfig.StartZoneConfig);
                    break;
                case SceneType.Gate:
                    scene.AddComponent<NetKcpComponent, IPEndPoint>(startSceneConfig.OuterIPPort);
                    scene.AddComponent<UserComponent>();
                    scene.AddComponent<GateSessionKeyComponent>();
                    scene.AddComponent<DBComponent,StartZoneConfig>(startSceneConfig.StartZoneConfig);
                    break;
                case SceneType.Map:
                    scene.AddComponent<UnitComponent>();
                    scene.AddComponent<RecastPathComponent>();
                    scene.AddComponent<DBComponent,StartZoneConfig>(startSceneConfig.StartZoneConfig);
                    break;
                case SceneType.Location:
                    scene.AddComponent<LocationComponent>();
                    break;
                case SceneType.DBCache:
                    scene.AddComponent<DBCacheComponent>();
                    scene.AddComponent<DBComponent,StartZoneConfig>(startSceneConfig.StartZoneConfig);
                    break;
            }

            return scene;
        }
    }
}