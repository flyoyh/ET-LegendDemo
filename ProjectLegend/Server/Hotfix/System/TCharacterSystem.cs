﻿namespace ET
{
    [ObjectSystem]
    public class TCharacterAwakeSystem : AwakeSystem<TCharacter, long, string>
    {
        public override void Awake(TCharacter self, long userId, string characterName)
        {
            self.UserId = userId;
            self.CharacterName = characterName;
            self.CharacterClass = 0;
            self.Gender = 0;
            self.Level = 1;
            self.Gold = 1000;
            self.Gem = 0;
        }
    }
}
