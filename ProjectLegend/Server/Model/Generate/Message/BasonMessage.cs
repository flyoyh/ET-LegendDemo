using ET;
using ProtoBuf;
using System.Collections.Generic;
namespace ET
{
	[ResponseType(typeof(D2G_GetComponent))]
	[Message(BasonOpcode.G2D_GetComponent)]
	[ProtoContract]
	public partial class G2D_GetComponent: Object, IDBCacheActorRequest
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(93)]
		public long ActorId { get; set; }

		[ProtoMember(1)]
		public string Component { get; set; }

		[ProtoMember(2)]
		public long CharacterId { get; set; }

	}

	[Message(BasonOpcode.D2G_GetComponent)]
	[ProtoContract]
	public partial class D2G_GetComponent: Object, IDBCacheActorResponse
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(91)]
		public int Error { get; set; }

		[ProtoMember(92)]
		public string Message { get; set; }

		[ProtoMember(1)]
		public Entity Component { get; set; }

	}

	[ResponseType(typeof(D2G_GetUnit))]
	[Message(BasonOpcode.G2D_GetUnit)]
	[ProtoContract]
	public partial class G2D_GetUnit: Object, IDBCacheActorRequest
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(93)]
		public long ActorId { get; set; }

		[ProtoMember(1)]
		public string Component { get; set; }

		[ProtoMember(2)]
		public long CharacterId { get; set; }

	}

	[Message(BasonOpcode.D2G_GetUnit)]
	[ProtoContract]
	public partial class D2G_GetUnit: Object, IDBCacheActorResponse
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(91)]
		public int Error { get; set; }

		[ProtoMember(92)]
		public string Message { get; set; }

		[ProtoMember(1)]
		public List<Entity> Components = new List<Entity>();

	}

	[ResponseType(typeof(D2M_SaveComponent))]
	[Message(BasonOpcode.M2D_SaveComponent)]
	[ProtoContract]
	public partial class M2D_SaveComponent: Object, IDBCacheActorRequest
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(93)]
		public long ActorId { get; set; }

		[ProtoMember(1)]
		public Entity Component { get; set; }

		[ProtoMember(2)]
		public long CharacterId { get; set; }

	}

	[Message(BasonOpcode.D2M_SaveComponent)]
	[ProtoContract]
	public partial class D2M_SaveComponent: Object, IDBCacheActorResponse
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(91)]
		public int Error { get; set; }

		[ProtoMember(92)]
		public string Message { get; set; }

	}

	[ResponseType(typeof(D2M_SaveUnit))]
	[Message(BasonOpcode.M2D_SaveUnit)]
	[ProtoContract]
	public partial class M2D_SaveUnit: Object, IDBCacheActorRequest
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(93)]
		public long ActorId { get; set; }

		[ProtoMember(1)]
		public List<Entity> Components = new List<Entity>();

		[ProtoMember(2)]
		public long CharacterId { get; set; }

	}

	[Message(BasonOpcode.D2M_SaveUnit)]
	[ProtoContract]
	public partial class D2M_SaveUnit: Object, IDBCacheActorResponse
	{
		[ProtoMember(90)]
		public int RpcId { get; set; }

		[ProtoMember(91)]
		public int Error { get; set; }

		[ProtoMember(92)]
		public string Message { get; set; }

	}

}
