using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using ProtoBuf;

namespace ET
{
    [ProtoContract]
    [Config]
    public partial class EquipConfigCategory : ProtoObject
    {
        public static EquipConfigCategory Instance;
		
        [ProtoIgnore]
        [BsonIgnore]
        private Dictionary<int, EquipConfig> dict = new Dictionary<int, EquipConfig>();
		
        [BsonElement]
        [ProtoMember(1)]
        private List<EquipConfig> list = new List<EquipConfig>();
		
        public EquipConfigCategory()
        {
            Instance = this;
        }
		
		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            foreach (EquipConfig config in list)
            {
                this.dict.Add(config.Id, config);
            }
            list.Clear();
            this.EndInit();
        }
		
        public EquipConfig Get(int id)
        {
            this.dict.TryGetValue(id, out EquipConfig item);

            if (item == null)
            {
                throw new Exception($"配置找不到，配置表名: {nameof (EquipConfig)}，配置id: {id}");
            }

            return item;
        }
		
        public bool Contain(int id)
        {
            return this.dict.ContainsKey(id);
        }

        public Dictionary<int, EquipConfig> GetAll()
        {
            return this.dict;
        }

        public EquipConfig GetOne()
        {
            if (this.dict == null || this.dict.Count <= 0)
            {
                return null;
            }
            return this.dict.Values.GetEnumerator().Current;
        }
    }

    [ProtoContract]
	public partial class EquipConfig: ProtoObject, IConfig
	{
		[ProtoMember(1, IsRequired  = true)]
		public int Id { get; set; }
		[ProtoMember(2, IsRequired  = true)]
		public string Name { get; set; }
		[ProtoMember(3, IsRequired  = true)]
		public int Slot { get; set; }
		[ProtoMember(4, IsRequired  = true)]
		public int MinADAdd { get; set; }
		[ProtoMember(5, IsRequired  = true)]
		public int MaxADAdd { get; set; }
		[ProtoMember(6, IsRequired  = true)]
		public int MinAPAdd { get; set; }
		[ProtoMember(7, IsRequired  = true)]
		public int MaxAPAdd { get; set; }
		[ProtoMember(8, IsRequired  = true)]
		public int MinDSAdd { get; set; }
		[ProtoMember(9, IsRequired  = true)]
		public int MaxDSAdd { get; set; }
		[ProtoMember(10, IsRequired  = true)]
		public int MinDefAdd { get; set; }
		[ProtoMember(11, IsRequired  = true)]
		public int MaxDefAdd { get; set; }
		[ProtoMember(12, IsRequired  = true)]
		public int MinMDefAdd { get; set; }
		[ProtoMember(13, IsRequired  = true)]
		public int MaxMDefAdd { get; set; }
		[ProtoMember(14, IsRequired  = true)]
		public int HitAdd { get; set; }
		[ProtoMember(15, IsRequired  = true)]
		public int MissAdd { get; set; }
		[ProtoMember(16, IsRequired  = true)]
		public int APMissAdd { get; set; }
		[ProtoMember(17, IsRequired  = true)]
		public int AtkSpeedPct { get; set; }
		[ProtoMember(18, IsRequired  = true)]
		public int LuckeyAdd { get; set; }
		[ProtoMember(19, IsRequired  = true)]
		public int HPAdd { get; set; }
		[ProtoMember(20, IsRequired  = true)]
		public int SuckHPAdd { get; set; }
		[ProtoMember(21, IsRequired  = true)]
		public bool HasHideAttr { get; set; }
		[ProtoMember(22, IsRequired  = true)]
		public int HideAttrID { get; set; }


		[ProtoAfterDeserialization]
        public void AfterDeserialization()
        {
            this.EndInit();
        }
	}
}
