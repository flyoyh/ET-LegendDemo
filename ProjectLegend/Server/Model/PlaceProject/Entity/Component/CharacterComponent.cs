﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：CharacterComponent
     * 创建时间：2021/7/17 星期六 21:09:48
     * 功    能：
*****************************************************/
using System.Collections.Generic;
using System.Linq;

namespace ET
{
    /// <summary>
    /// 在线Character对象管理组件
    /// </summary>
    public class CharacterComponent : Entity
    {
        private readonly Dictionary<long, TCharacter> idCharacters = new Dictionary<long, TCharacter>();

        /// <summary>
        /// 添加Character对象
        /// </summary>
        /// <param name="character"></param>
        public void Add(TCharacter character)
        {
            if (Get(character.Id) == null)
            {
                this.idCharacters.Add(character.Id, character);
            }
            else
            {
                Log.Error("重复登录！");
            }
        }

        /// <summary>
        /// 获取Character对象
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TCharacter Get(long id)
        {
            this.idCharacters.TryGetValue(id, out TCharacter Character);
            return Character;
        }

        /// <summary>
        /// 移除Character对象
        /// </summary>
        /// <param name="id"></param>
        public void Remove(long id)
        {
            this.idCharacters.Remove(id);
        }

        /// <summary>
        /// Character对象总数量
        /// </summary>
        public int Count
        {
            get
            {
                return this.idCharacters.Count;
            }
        }

        /// <summary>
        /// 获取所有Character对象
        /// </summary>
        /// <returns></returns>
        public TCharacter[] GetAll()
        {
            return this.idCharacters.Values.ToArray();
        }

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            base.Dispose();

            foreach (TCharacter Character in this.idCharacters.Values)
            {
                Character.Dispose();
            }
        }
    }
}