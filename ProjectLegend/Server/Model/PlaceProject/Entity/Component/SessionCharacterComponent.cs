﻿/****************************************************
     * 作    者：Leng
     * 邮    箱：2947672@qq.com
     * 文件名称：SessionCharacterComponent
     * 创建时间：2021/7/17 星期六 21:08:35
     * 功    能：
*****************************************************/
namespace ET
{
    public class SessionCharacterComponent : Entity
    {
        public TCharacter Character { get; set; }
    }
}

