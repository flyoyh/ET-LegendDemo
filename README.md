# ET-LegendDemo

#### 介绍
ET框架传奇DEMO工程，ET6的DEMO和文档太少了，就把自己正在做的的工程分享出来吧
作者也是菜鸟，刚学没多久，代码水平一般，见谅，有如错误请指正。

#### 架构
基于字母哥的CSharp.lua分支
集成了SJ大佬的缓存服
集成了DECT的FGUI
集成了XAsset4 (因为临时调换的，只能跑，其他功能需要自行调整)

#### 包含内容
包含了注册登陆、角色创建、进入游戏、背包、装备、人物属性、商店功能、缓存服获取信息更新信息
包含了简单的MongoDB的使用、服务器通讯、Actor消息、配置表、数值组件、缓存服等。

#### 运行教程
1.  打开Unity  Assets-Open C# Project 全部编译，完成后回到Unity界面Shift+S生成Lua代码
2.  打开Client-Server.sln 全部编译
2.  客户端初始场景为_Scene/NormalScene/HotUpdate
3.  MongoDB数据库名称PlaceLegend，可自行在配置表中修改
